<?php

/* Clase encargada de gestionar las conexiones a la base de datos */

class Db {

    private $servidor;
    private $usuario;
    private $password;
    private $base_datos;
    private $link;
    private $stmt;
    private $array;
    static $_instance;

    /* La función construct es privada para evitar que el objeto pueda ser creado mediante new */

    private function __construct() {
        $this->setConexion();
        $this->conectar();
    }

    /* Método para establecer los par�metros de la conexi�n */

    private function setConexion() {
        require BD_PATH . "/Conf.class.php";
        $conf = Conf::getInstance();
        $this->servidor = $conf->getHostDB();
        $this->base_datos = $conf->getDB();
        $this->usuario = $conf->getUserDB();
        $this->password = $conf->getPassDB();
    }

    /* Evitamos el clonaje del objeto. Patr�n Singleton */

    private function __clone() {
        
    }

    /* Funci�n encargada de crear, si es necesario, el objeto. Esta es la funci�n que debemos llamar desde fuera de la clase para instanciar el objeto, y as�, poder utilizar sus m�todos */

    public static function getInstance() {
        if (!(self::$_instance instanceof self))
            self::$_instance = new self();
        return self::$_instance;
    }

    /* Realiza la conexi�n a la base de datos. */

    private function conectar() {
        $this->link = new mysqli($this->servidor, $this->usuario, $this->password);
        $this->link->select_db($this->base_datos);
        @$this->link->set_charset('utf8');
    }

    /* M�todo para ejecutar una sentencia sql */

    public function ejecutar($sql) {
        $this->stmt = $this->link->query($sql);
        return $this->stmt;
    }

    /* M�todo para obtener una fila de resultados de la sentencia sql */

    public function obtener_fila($stmt, $fila = null) {
        if (is_null($fila)) {
            $data_array = array();
            while ($row = $stmt->fetch_array()) {
                array_push($data_array, $row);
            }
            return $data_array;
        } else {
            if ($fila == 0) {
                $this->array = $this->stmt->fetch_array();
            } else {
                $this->stmt->data_seek($fila);
                $this->array = $this->stmt->fetch_array();
            }
            return $this->array;
        }
    }

    //Devuelve el �ltimo id del insert introducido
    public function lastID() {
        return $this->link->insert_id;
    }

}

?>
