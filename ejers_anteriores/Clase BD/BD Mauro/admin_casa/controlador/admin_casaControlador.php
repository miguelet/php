<?php

class Admin_casaControlador {

    private $casa;

    public function __construct() {
        
    }

    function casa() {
        require CASA_MODELO . '/modelo/dao/casadao.php';
        $casa = casadao::getInstance();
        $items = $casa->obtenercasas();
        if (count($items) > 0) {
            require CASA_MODELO_ADMIN . '/vista/listar.php';
        } else {
            require CASA_MODELO_ADMIN . '/vista/noitems.php';
        }
    }

    function addcasa() {
        require CASA_MODELO . '/modelo/dao/casadao.php';
        $casa = casadao::getInstance();
        if (isset($_POST['creandocasa'])) {
            $error = "";
            $nombreCarpeta = "/img/";
            require_once MODEL . '/common.php';
            $common = common::getInstance();
            if ($nombreFichero = $common->subirimagen($_FILES, $nombreCarpeta, $error)) {
                $ruta = $nombreCarpeta . $nombreFichero;
                if ($casa->insertarcasa($_POST['nombre'], 1, $ruta, $_POST['descripcion'])) {
                    echo '<script>window.location = "' . SITE_PATH_ADMIN . '?controlador=casa";</script>';
                }
            } else {
                $error .= "<p>Error al insertar casa.</p>";
            }
        }
        $error = error($error);
        require CASA_MODELO_ADMIN . '/vista/addcasa.php';
    }

    function editcasa() {
        $error = "";
        require CASA_MODELO . '/modelo/dao/casadao.php';
        $casa = casadao::getInstance();
        $id = $_GET['id'];
        if (isset($_POST['editandocasa'])) {

            if ($casa->editarcasa($_POST['editandocasa'], $_POST['nombre'], $_POST['descripcion'])) {
                if ($_FILES['imagen']['name'] != "" && $_FILES['imagen']['size'] > 0) {
                    $data = $casa->obtenercasa($_POST['editandocasa']);
                    if (unlink(MODULOS_CASA_VISTA . $data['img'])) {
                        $nombreCarpeta = "/img/";
                        require_once MODEL . '/common.php';
                        $common = common::getInstance();
                        if ($nombreFichero = $common->subirimagen($_FILES, $nombreCarpeta, $error)) {
                            $casa->editarcasaimg($_POST['editandocasa'], $nombreCarpeta . $_FILES['imagen']['name']);
                        }
                    }
                }
                echo '<script>window.location = "' . SITE_PATH_ADMIN . '?controlador=casa";</script>';
            } else {
                $error .= "Error al editar.";
                $id = $_POST['editandocasa'];
            }
        } else {
            $data = $casa->obtenercasa($id);
            $error = error($error);
            require CASA_MODELO_ADMIN . '/vista/detalles.php';
        }
    }

    function eliminar() {
        $error = "";
        require CASA_MODELO . '/modelo/dao/casadao.php';
        $casa = casadao::getInstance();
        $id = $_GET['id'];
        $data = $casa->obtenercasa($id);

        $filename = MODULOS_CASA_VISTA . $data['img'];

        if ($casa->eliminarcasa($id)) {
            echo '<script>window.location = "' . SITE_PATH_ADMIN . '?controlador=casa";</script>';
        } else {
            $error.="<p>No se ha podido eliminar.</p>";
        }
        if (file_exists($filename)) {
            unlink($filename);
        }
        $error = error($error);
    }

}

?>
