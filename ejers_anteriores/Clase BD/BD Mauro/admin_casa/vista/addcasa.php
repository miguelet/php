<div id="content">
    <div id="addcasa">
        <form name="addcasa" method="post" ENCTYPE="multipart/form-data">
            <?php
            echo $error;
            ?>
            <input name="creandocasa" type="hidden" value="true">
            <label for="nombre">Nombre: <span class="required">*</span></label>
            <input name="nombre" id="nombre" type="text" value="<?php echo $_POST['nombre'] ?>">
            <label for="descripcion">Descripción: <span class="required">*</span></label>
            <textarea name="descripcion" id="descripcion"><?php echo $_POST['descripcion'] ?></textarea>

            <label>Imagen:</label>
            <input name="MAX_FILE_SIZE" type="HIDDEN" value="5120000" />
            <input name="imagen" size="44" type="FILE" />

            <input name="submit_addcasa" id="submit_addcasa" type="submit">
        </form>
    </div>
</div>
<script src="<?php echo SITE_PATH; ?>/modulos/admin_casa/vista/js/form.js" type="text/javascript"></script>