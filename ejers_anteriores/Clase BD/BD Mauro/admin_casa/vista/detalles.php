<div id="content">
    <div id="editcasa">
        <form name="editcasa" method="post" ENCTYPE="multipart/form-data">
            <?php
            echo $error;
            ?>
            <input name="editandocasa" type="hidden" value="<?php echo $_GET['id']; ?>">
            <label for="nombre">Nombre: <span class="required">*</span></label>
            <input name="nombre" id="nombre" type="text" value="<?php echo $data['nombre'] ?>">
            <label for="descripcion">Descripción: <span class="required">*</span></label>
            <textarea name="descripcion" id="descripcion"><?php echo $data['descripcion'] ?></textarea>

            <label>Imagen:</label>
            <div class="img_editcasa">
                <img src="<?php echo SITE_PATH_TIMTHUMB . '/timthumb.php?src=' . SITE_PATH_CASA . $data['img'] . '&h=200&w=450' ?>" alt="<?php echo $value['nombre'] ?>"/>
            </div>
            <input name="MAX_FILE_SIZE" type="HIDDEN" value="5120000" />
            <input name="imagen" size="44" type="FILE" />

            <input name="submit_edit_casa" id="submit_edit_casa" type="submit">
        </form>
    </div>
</div>
<script src="<?php echo SITE_PATH; ?>/modulos/admin_casa/vista/js/detalles.js" type="text/javascript"></script>