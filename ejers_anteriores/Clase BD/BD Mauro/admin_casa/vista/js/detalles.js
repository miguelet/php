var validator = new FormValidator('editcasa', [{
        name: 'nombre',
        display: 'nombre',
        rules: 'required'
    }, {
        name: 'descripcion',
        display: 'descripcion',
        rules: 'required'
    }], function(errors, event) {
    event.preventDefault();
    if (errors.length > 0) {
        var nodeerror = document.getElementById("error");
        if (nodeerror != null) {
            nodeerror.remove();
        }
        var errorString = '';
        var diverror = document.createElement("div");
        diverror.id = "error";
        for (var i = 0, errorLength = errors.length; i < errorLength; i++) {
            var p = document.createElement("p");
            var text = document.createTextNode(errors[i].message);
            p.appendChild(text);
            diverror.appendChild(p);
        }
        var submit = document.getElementById("submit_edit_casa");
        submit.parentNode.insertBefore(diverror, submit);
        submit.parentNode.insertBefore;
    } else {
        var controlador = "?controlador=casa&accion=editcasa";
        document.forms[0].action = controlador;
        document.forms[0].submit();
    }
});
