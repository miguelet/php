<div id="content">
    <div class="add_casa">
        <a href="?controlador=casa&accion=addcasa">A&ntilde;adir casa</a>
    </div>
    <div id="casas">
        <?php foreach ($items as $key => $value) { ?>
            <div class="casa">
                <img src="<?php echo SITE_PATH_TIMTHUMB . '/timthumb.php?src=' . SITE_PATH_CASA . $value['img'] . '&h=150&w=150' ?>" alt="<?php echo $value['nombre'] ?>"/>
                <h1><?php echo $value['nombre'] ?></h1>
                <a href="<?php echo SITE_PATH_ADMIN . '?controlador=casa&accion=editcasa&id=' . $value['id']; ?>"><img src="<?php echo SITE_PATH_ADMINCASA ?>/img/edit.png" /></a>
                <a href="<?php echo SITE_PATH_ADMIN . '?controlador=casa&accion=eliminar&id=' . $value['id']; ?>" onclick="return confirm('Desea eliminar la casa?');"><img src="<?php echo SITE_PATH_ADMINCASA ?>/img/delete.png" /></a>
            </div>
        <?php } ?>
    </div>
</div>