<?php

require_once BD_PATH . '/Db.class.php';

class casadao {

    private $db;
    static $_instance;

    public function __construct() {
        $this->db = Db::getInstance();
    }

    public static function getInstance() {
        if (!(self::$_instance instanceof self))
            self::$_instance = new self();
        return self::$_instance;
    }

    public function obtenercasas() {
        $stmt = $this->db->ejecutar('SELECT * FROM ' . CASAS);
        return $this->db->obtener_fila($stmt);
    }

    public function obtenercasa($id) {
        $stmt = $this->db->ejecutar('SELECT * FROM ' . CASAS . ' WHERE id=' . $id);
        return $this->db->obtener_fila($stmt, 0);
    }

    public function editarcasa($id, $nombre, $descripcion) {
        $stmt = $this->db->ejecutar("UPDATE " . CASAS . " SET nombre='$nombre', descripcion='$descripcion' WHERE id='$id';");
        return $stmt;
    }

    public function editarcasaimg($id, $img) {
        $stmt = $this->db->ejecutar("UPDATE " . CASAS . " SET img='$img' WHERE id='$id';");
        return $stmt;
    }

    public function insertarcasa($nombre, $tipo, $img, $descripcion) {
        $stmt = $this->db->ejecutar("INSERT INTO " . CASAS . " (nombre, tipo, img, descripcion) VALUES ('$nombre', '$tipo', '$img', '$descripcion');");
        return $stmt;
    }

    public function eliminarcasa($id) {
        $stmt = $this->db->ejecutar("DELETE FROM " . CASAS . " WHERE id='$id';");
        return $stmt;
    }

}

?>
