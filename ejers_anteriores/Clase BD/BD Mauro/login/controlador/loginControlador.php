<?php

require LOGIN_MODELO . '/modelo/dao/logindao.php';

class LoginControlador {

    private $login;
    private $cipher;

    public function __construct() {
        
    }

    function login() {
        $login = logindao::getInstance();
        session_start();

        if (!isset($_SESSION['idusuario'])) {
            if (isset($_POST['logeando'])) {
                require_once LIB . '/cifrado/Cipher.php';
                $this->cipher = Cipher::getInstance();
                $passwd = $this->cipher->encrypt($_POST['passwd']);
                $stmt = $login->comprobar_usuario($_POST['usuario'], $passwd);
                if (count($stmt) > 0) {
                    $_SESSION['usuario'] = $stmt['usuario'];
                    $_SESSION['idusuario'] = $stmt['id'];
                    $_SESSION['tipousuario'] = $stmt['tipo'];
                    echo '<script>window.location = "' . ROOT_SITE_PATH . '";</script>';
                } else {
                    $error = '<p>Su usuario es incorrecto, intente nuevamente.</p>';
                    $error = error($error);
                    include LOGIN_MODELO . '/vista/form.php';
                }
            } else {
                include LOGIN_MODELO . '/vista/form.php';
            }
        } else {
            echo '<script>window.location = "' . ROOT_SITE_PATH . '";</script>';
        }
    }

    function olvidadopassword() {
        $login = logindao::getInstance();
        if (isset($_POST['submit_olvidadopassword'])) {
            $val_email = filter_var($_POST['email'], FILTER_VALIDATE_EMAIL);
            if (!$val_email) {
                $error.="<p>El correo electronico no es correcto.</p>";
            } else {
                $datos_usu = $login->existe_usuario_correo($_POST['email']);
                if (is_null($datos_usu)) {
                    $error.="<p>Este correo no est&aacute; registrado.</p>";
                } else {
                    require_once LIB . '/PHPMailer_v5.1/Email.class.php';

                    $longitud = 32;
                    do {
                        $pass = substr(md5(microtime()), 1, $longitud);
                        $token = ROOT_SITE_PATH . "?controlador=login&accion=recuperacionpassword&recuperacionpassword=$pass";
                    } while (count($login->existe_usuario_recuperacionpass($pass)) > 0);

                    $login_email = Email::getInstance();
                    $login_email->subject = "Recuperación de la contraseña.";
                    $login_email->nombre = $datos_usu['nombre'] . ' ' . $datos_usu['apellidos'];
                    $login_email->body = "Pulse <a href='$token'>aquí</a> para restaurar su contrase&ntilde;a.";
                    $login_email->address = $_POST['email'];
                    $login_email->enviar();
                    $login->usuario_recuperacionpass_update($pass, $datos_usu['id']);
                    include LOGIN_MODELO . '/vista/correoenviado.php';
                    return false;
                }
            }
        }
        $error = error($error);
        include LOGIN_MODELO . '/vista/olvidadopassword.php';
    }

    function recuperacionpassword() {
        if (isset($_GET['recuperacionpassword'])) {
            $login = logindao::getInstance();
            $data = $login->existe_usuario_recuperacionpass($_GET['recuperacionpassword']);
            if (count($data) > 0) {
                include LOGIN_MODELO . '/vista/form_nuevopassword.php';
            } else {
                include LOGIN_MODELO . '/vista/errorpage.php';
            }
            $error = error($error);
        }
    }

    function recuperandopasswd() {
        if (isset($_POST['submit_recuperandopasswd'])) {
            if ($_POST['passwd'] == $_POST['new_passwd']) {
                $login = logindao::getInstance();
                $login->usuario_recuperacionpass_update(NULL, $_POST['idusuario']);
                require_once LIB . '/cifrado/Cipher.php';
                $this->cipher = Cipher::getInstance();
                $pass = $this->cipher->encrypt($_POST['passwd']);
                $login->insertar_nuevo_password($pass, $_POST['idusuario']);
            } else {
                $error.="<p>Las contrase&ntilde;as no coinciden.</p>";
            }
        }
        $error = error($error);
    }

    function cerrarsesion() {
        session_start();
        $_SESSION['usuario'] = NULL;
        $_SESSION['idusuario'] = NULL;
        session_destroy();
        echo '<script>window.location = "' . ROOT_SITE_PATH . '?controlador=login";</script>';
    }

}

?>
