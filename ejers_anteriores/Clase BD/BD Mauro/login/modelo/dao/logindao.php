<?php

require_once BD_PATH . '/Db.class.php';

class logindao {

    private $db;
    static $_instance;

    public function __construct() {
        $this->db = Db::getInstance();
    }

    public static function getInstance() {
        if (!(self::$_instance instanceof self))
            self::$_instance = new self();
        return self::$_instance;
    }

    public function comprobar_usuario($usuario, $password) {
        $sql = "SELECT * FROM usuarios WHERE usuario='$usuario' AND  password='$password' AND  activo=1";
        $stmt = $this->db->ejecutar($sql);
        return $this->db->obtener_fila($stmt, 0);
    }

    public function existe_usuario_correo($correo) {
        $sql = "SELECT * FROM usuarios WHERE email='$correo';";
        $stmt = $this->db->ejecutar($sql);
        return $this->db->obtener_fila($stmt, 0);
    }

    public function existe_usuario_recuperacionpass($recuperacionpass) {
        $sql = "SELECT * FROM  usuarios WHERE  recuperacionpass =  '$recuperacionpass'";
        $stmt = $this->db->ejecutar($sql);
        return $this->db->obtener_fila($stmt);
    }
    
    public function usuario_recuperacionpass_update($recuperacionpass, $id) {
        $sql = "UPDATE usuarios SET recuperacionpass='$recuperacionpass' WHERE id='$id';";
        $stmt = $this->db->ejecutar($sql);
        return $stmt;
    }
    
    public function insertar_nuevo_password($pass, $id) {
        $sql = "UPDATE usuarios SET password='$pass' WHERE id='$id'";
        $stmt = $this->db->ejecutar($sql);
        return $stmt;
    }
}

?>
