<div id="sections">
    <setion>
        <div class="center">
            <header>
                <h1>Iniciar sesi&oacute;n</h1>
            </header>
            <div class="content">
                <div id="login">
                    <form name="login" method="post">
                        <input type="hidden" name="logeando" value="true" />
                        <label for="usuario">Usuario:</label>
                        <input name="usuario" id="usuario" type="text"/>
                        <label for="passwd">Contrase&ntilde;a:</label>
                        <input name="passwd" id="passwd" type="password"/>
                        <?php echo $error; ?>
                        <input name="submit_login" id="submit_login" type="submit" />
                    </form>
                    <div class="registrarse">
                        <p>
                            <a href="<?php echo ROOT_SITE_PATH; ?>?controlador=login&accion=olvidadopassword">He olvidado mi contrase&ntilde;a</a> | No est&aacute; registrado? <a href="<?php echo ROOT_SITE_PATH; ?>?controlador=registro">Reg&iacute;strese.</a>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </setion>
</div>
<script src="<?php echo SITE_PATH; ?>/modulos/login/vista/js/form.js" type="text/javascript"></script>
<script src="<?php echo SITE_PATH; ?>/vista/lib/serialize-0.2.js" type="text/javascript"></script>