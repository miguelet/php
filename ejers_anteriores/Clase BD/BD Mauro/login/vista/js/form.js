var validator = new FormValidator('login', [{
        name: 'usuario',
        display: 'Campo usuario',
        rules: 'required'
    }, {
        name: 'passwd',
        display: 'Campo password',
        rules: 'required'
    }], function(errors, event) {
    event.preventDefault();
    if (errors.length > 0) {
        var nodeerror = document.getElementById("error");
        if (nodeerror != null) {
            nodeerror.remove();
        }
        var errorString = '';
        var diverror = document.createElement("div");
        diverror.id = "error";
        for (var i = 0, errorLength = errors.length; i < errorLength; i++) {
            var p = document.createElement("p");
            var text = document.createTextNode(errors[i].message);
            p.appendChild(text);
            diverror.appendChild(p);
        }
        var submit_login = document.getElementById("submit_login");
        submit_login.parentNode.insertBefore(diverror, submit_login);
        submit_login.parentNode.insertBefore;
    } else {
        var controlador = "index.php?controlador=login";
        document.forms[0].action = controlador;
        document.forms[0].submit();
//        var send = serialize(document.forms[0]);
//        var xmlhttp = null;
//        if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
//            xmlhttp = new XMLHttpRequest();
//        } else {// code for IE6, IE5
//            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
//        }
//        xmlhttp.onreadystatechange = function() {
//            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
//                alert(xmlhttp.responseText);
//                if (xmlhttp.responseText == "ok") {
//                    window.location = "index.php";
//                }else{
//                    document.getElementById("wrapper").innerHTML = xmlhttp.responseText;
//                }
//            }
//        }
//        xmlhttp.open("POST", controlador, true);
//        xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
//        xmlhttp.send(send);
    }
});
