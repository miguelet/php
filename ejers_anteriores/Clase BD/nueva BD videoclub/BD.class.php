<?php
	class Excepcion_Transaccion extends Exception{
		public function __construct(){}
	}
	
	/* Clase encargada de gestionar las conexiones a la base de datos */
	class BD {
		private $servidor;
		private $usuario;
		private $password;
		private $base_datos;
		private $link;
		private $stmt;
		private $array;
		static $_instance;

		/* La funci�n construct es privada para evitar que el objeto pueda ser creado mediante new */
		private function __construct() {
			include ('conf.class.php');
			$this->setConexion();
		}

		/* M�todo para establecer los par�metros de la conexi�n */
		private function setConexion() {
			$conf = conf::getInstance();
			$this->servidor = $conf->getHostDB();
			$this->base_datos = $conf->getDB();
			$this->usuario = $conf->getUserDB();
			$this->password = $conf->getPassDB();
		}

		/* Evitamos el clonaje del objeto. Patr�n Singleton */
		private function __clone() {
		}

		/* Funci�n encargada de crear, si es necesario, el objeto. Esta es la funci�n que debemos llamar desde fuera de la clase para instanciar el objeto, y as�, poder utilizar sus m�todos */
		public static function getInstance() {
			if (!(self::$_instance instanceof self))
				self::$_instance = new self();
			return self::$_instance;
		}

		/* Realiza la conexi�n a la base de datos. */
		public function conectar() {
			try{
				$this->link = new mysqli($this->servidor, $this->usuario, $this->password);
				if (mysqli_connect_errno() != 0)
					throw new Exception('Error conectando: '.mysqli_connect_error(), mysqli_connect_errno());
				
				$this->link->select_db($this->base_datos);
				if ($this->link->errno != 0)
					throw new Exception('Error seleccionando bd: '.$this->link->error, $this->link->errno);
			
			}catch (Exception $e){
				echo $e->getMessage();
				$this->desconectar();
				exit();
			}
		}
		
		public function desconectar() {
			$this->link->close();
		}

		public function ejecutar($sql) { //select -> devuelve un array
			$arr = array();
			try{
				$this->stmt = $this->link->query($sql);
				if ($this->link->errno != 0)
					throw new Exception('Error realizando consulta: '.$this->link->error, $this->link->errno);
				
				if ($this->stmt->num_rows > 0){
					while ($row = $this->stmt->fetch_row())
						$arr[] = $row;
				}
				$this->stmt->free();
				return $arr;
					
			}catch (Exception $e){
				echo $e->getMessage();
				$this->stmt->free();
				$this->desconectar();
				exit();
			}
		}
		
		public function consulta($sql) { //p.e. insert into, update, delete  -> devuelve true/false
			$rdo=false;
			try{
				$this->stmt = $this->link->query($sql);
				if ($this->link->errno != 0)
					throw new Exception('Error realizando consulta: '.$this->link->error, $this->link->errno);

				if ($this->stmt === false){
					throw new Excepcion_Transaccion();
				}else{
					$rdo=true;
					$this->link->commit();
				}
				
			}catch (ExcepcionEnTransaccion $e){
				echo 'No se ha podido realizar la operación';
				$this->stmt->free();
				$this->stmt->rollback();
				$this->desconectar();
			}catch (Exception $e){
				echo $e->getMessage();
				$this->stmt->free();
				$this->desconectar();
				exit();
			}
			return $rdo;
		}

		public function obtener_filas($sql) { //devuelve el nº de filas afectadas
			$rdo=0;
			try{
				$this->stmt = $this->link->query($sql);
				if ($this->link->errno != 0)
					throw new Exception('Error realizando consulta: '.$this->link->error, $this->link->errno);
				
				if ($this->stmt->num_rows > 0){
					$rdo=$this->stmt->num_rows;
				}
				
				$this->stmt->free();
				return $rdo;
				
			}catch (Exception $e){
				echo $e->getMessage();
				$this->stmt->free();
				$this->desconectar();
				exit();
			}
		}
	}
?>