<?php
	require("peliculas/MODELO/DAO/peliculasDAO.php");
	require ("MODELO/Conexion/BD.class.php");
	
	class peliculasBLL {
		function listarPeliculasBLL($arrArgument) {
			$bd = BD::getInstance();
			$arr = array();
			
		    $bd->conectar();
			$new_list = new PeliculasDAO();
			$arr = $new_list->listarPeliculasDAO($bd, $arrArgument);
			$bd->desconectar();
			return $arr;
		}

		function detallesPeliculaBLL($arrArgument) {
			$bd = BD::getInstance();
			$arr = array();

			$bd->conectar();
			$detalle_peli = new PeliculasDAO();
			$arr = $detalle_peli->detallesPeliculaDAO($bd, $arrArgument);
			$bd->desconectar();
			return $arr;
		}

		function peliculasOrdenadasBLL($genero) {
			$bd = BD::getInstance();
			$arr = array();

			$bd->conectar();
			$lista_peli = new PeliculasDAO();
			$arr = $lista_peli->peliculasOrdenadasDAO($bd, $genero);
			$bd->desconectar();
			return $arr;
		}

		function count_pelisBLL() {
			$bd = BD::getInstance();
			$arr = array();

			$bd->conectar();
			$count = new PeliculasDAO();
			$arr = $count->count_pelisDAO($bd);
			$bd->desconectar();
			return $arr;
		}
	}
