<div id="div_detalle_peliculas">
    <p class="titulo_div_general">Datos de la película <!--<?php echo utf8_encode($arrData[0][1]) ?>--></p>
    <div id="caja_detalle_pelicula">
        <?php
        if (isset($arrData) && !empty($arrData)) {
            foreach ($arrData as $pelicula) {
                ?>
                <div class="div_detalle_imagen">
                    <?php
                    $rutaImg = $pelicula[5];
                    ?>
                    <img src=<?php echo $rutaImg ?> />
                </div>

                <div class="datos_pelicula">
                    <p id="titulo_ficha">Ficha técnica</p>

                    <div id="datos_izquierda">
                        <p><span class="titulos_datos">Titulo:</span> <?php echo utf8_encode($pelicula[1]) ?></p>
                        <p><span class="titulos_datos">Director:</span> <?php echo utf8_encode($pelicula[2]) ?></p>
                        <p><span class="titulos_datos">Fecha estreno:</span> <?php echo $pelicula[3] ?></p>
                        <p><span class="titulos_datos">Género:</span> <?php echo utf8_encode($pelicula[4]) ?></p>
                        <p><span class="titulos_datos">Duración:</span> <?php echo $pelicula[6] ?> minutos</p>
                    </div>

                    <div id="datos_derecha">
                        <p><span class="titulos_datos">Actor principal:</span> <?php echo utf8_encode($pelicula[7]) ?></p>
                        <p><span class="titulos_datos">Idioma:</span> <?php echo utf8_encode($pelicula[9]) ?></p>
                    </div>

                </div>



                <div id="div_detalle_sinopsis">
                    <p id="titulo_sinopsis">Sinopsis de la película <?php echo utf8_encode($pelicula[1]) ?></p>
                    <p class="texto_sinopsis"><?php echo utf8_encode($pelicula[8]) ?></p>
                </div>

                
                    <div id="precio_pelicula">
                        <p id="texto_precio">Precio: <?php echo $pelicula[11] ?> € /dia</p>
                    </div>

                    <div id="boton_alquilar">
                        <a href="#"><img src="http://localhost:8088/videoclub/peliculas/VISTA/IMG/alq.png" alt="icono alquilar" title="Alquilar película"><span id="texto_alquilar">Alquilar</span></a>
                    </div>

                    <div id="boton_trailer">
                        <a href="<?php echo $pelicula[10] ?>" TARGET="_blank"><img src="http://localhost:8088/videoclub/peliculas/VISTA/IMG/youtube.png" alt="icono YouTube" title="Ver tráiler en YouTube"><span id="texto_trailer">Ver tráiler</span></a>
                    </div>

                    <div id="boton_volver">
                        <a href="index.php"><img src="http://localhost:8088/videoclub/peliculas/VISTA/IMG/volver.png" alt="icono volver" title="Volver a inicio"><span id="texto_volver">Ir a inicio</span></a>
                    </div>
                
                <?php
            }
        } else {
            echo 'La película con ese ID no existe';
        }
        ?>
    </div>
    <!--<div id="precio_pelicula">
            <p id="texto_precio">Precio: <?php echo $pelicula[11] ?> € /dia</p>
    </div>
    
    <div id="boton_alquilar">
            <a href="#"><img src="http://localhost:8088/videoclub/peliculas/VISTA/IMG/alq.png" alt="icono alquilar" title="Alquilar película"><span id="texto_alquilar">Alquilar</span></a>
    </div>
    
    <div id="boton_trailer">
            <a href="<?php echo $pelicula[10] ?>" TARGET="_blank"><img src="http://localhost:8088/videoclub/peliculas/VISTA/IMG/youtube.png" alt="icono YouTube" title="Ver tráiler en YouTube"><span id="texto_trailer">Ver tráiler</span></a>
    </div>
    
    <div id="boton_volver">
            <a href="index.php"><img src="http://localhost:8088/videoclub/peliculas/VISTA/IMG/volver.png" alt="icono volver" title="Volver a inicio"><span id="texto_volver">Ir a inicio</span></a>
    </div>-->
</div>