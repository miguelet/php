<?php
	require ("MODELO/Conexion/BD.class.php");
	require("usuarios/MODELO/DAO/usuariosDAO.php");

	class usuariosBLL {
		function cargarDNIusuarioBLL() {
			$bd = BD::getInstance();
			$arr = array();
			
			$bd->conectar();
			$lista_dni = new usuariosDAO();
			$arr = $lista_dni->cargarDNIusuarioDAO($bd);
			$bd->desconectar();
			return $arr;
		}

		function altaUsuarioBLL($arrArgument) {
			$bd = BD::getInstance();
			$rdo=false;
			
			$bd->conectar();
			$new_user = new usuariosDAO();
			$rdo = $new_user->altaUsuarioDAO($bd, $arrArgument);
			$bd->desconectar();
			return $rdo;
		}
		
		function loginUsuarioBLL($arrArgument){
			$bd = BD::getInstance();
			$rdo=0;
			
			$bd->conectar();
			$new_login = new usuariosDAO();
			$rdo = $new_login->loginUsuarioDAO($bd, $arrArgument); 
			$bd->desconectar();
			return $rdo;
		}
	}
