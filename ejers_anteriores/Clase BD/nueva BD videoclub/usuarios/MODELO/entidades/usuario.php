<?php
	class usuario {
		private $nick;
		private $password;
		private $nombre;
		private $apellidos;
		private $dni;
		private $fecha_nacimiento;
		private $edad;
		private $fecha_alta;
		private $telefono;
		private $email;
		private $ciudad;
		private $codigo_postal;
		private $tipo;
		private $saldo;
	   
		public function __construct($nick, $password, $nombre, $apellidos, $dni, $fecha_nacimiento, $edad, $fecha_alta, $telefono, $email, $ciudad, $codigo_postal, $tipo, $saldo) {
			$this->nick = $nick;
			$this->password = $password;
			$this->nombre = $nombre;
			$this->apellidos = $apellidos;
			$this->dni = $dni;
			$this->fecha_nacimiento = $fecha_nacimiento;
			$this->edad = $edad;
			$this->fecha_alta = $fecha_alta;
			$this->telefono = $telefono;
			$this->email = $email;
			$this->ciudad = $ciudad;
			$this->codigo_postal = $codigo_postal;
			$this->tipo = $tipo;
			$this->saldo = $saldo;
		}

		public function __get($property) {
			if (property_exists($this, $property)) {
				return $this->$property;
			}
		}

		public function __set($property, $value) {
			if (property_exists($this, $property)) {
				$this->$property = $value;
			}
		}

		public function __toString() {
			return "Usuario: " . $this->nick . "\n" .
					//"Contraseña: " . $this->password . "\n" .
					"Nombre: " . $this->nombre . "\n" .
					"Apellidos: " . $this->apellidos . "\n" .
					"DNI: " . $this->dni . "\n" .
					"Fecha nacimiento: " . $this->fecha_nacimiento . "\n" .
					"Edad: " . $this->edad . "\n" .
					"Fecha alta: " . $this->fecha_alta . "\n" .
					"Teléfono: " . $this->telefono . "\n" .
					"Correo electrónico: " . $this->email . "\n" .
					"Ciudad: " . $this->ciudad . "\n" .
					"Código postal: " . $this->codigo_postal . "\n" .
					"Tipo: " . $this->tipo . "\n" .
					"Saldo: " . $this->saldo . " €";
		}
	}
