<?php
	class modelo_usuarios {
		function cargarDNIusuario(){
			$new_list = new usuariosBLL();
			return $new_list->cargarDNIusuarioBLL();
		}
		
		function altaUsuario($arrArgument){
			$new_user = new usuariosBLL();
			return $new_user->altaUsuarioBLL($arrArgument);
		}
		
		function loginUsuario($arrArgument){
			$new_login = new usuariosBLL();
			return $new_login->loginUsuarioBLL($arrArgument);
		}
	}
