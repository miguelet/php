<?php
	/*date_default_timezone_set('Europe/Madrid');
	define("L_LANG", "es_ES");
	require_once('Calendar/tc_calendar.php');*/
?>

<div id="div_usuarios">
    <p class="titulo_div_general">Administrar usuarios</p>

    <form action="index.php?CONTROLADOR=controlador_usuarios&function=alta_usuario" method="POST" onsubmit="return validacion()">
        <div class='div_formulario'>
            <div class='izquierda'>
                <label for='dni'>DNI</label>
                <input type='text' id='dni' name='dni' placeholder="DNI">

                <label for='nick'>Usuario</label>
                <input type='text' id='nick' name='nick' placeholder="Usuario">

                <label for='pass'>Contraseña</label>
                <input type='password' id='pass' name='pass' placeholder="Contraseña">

                <label for='nombre'>Nombre</label>
                <input type='text' id='nombre' name='nombre' placeholder="Nombre">

                <label for='apellido'>Apellido</label>
                <input type='text' id='apellido' name='apellido' placeholder="Apellido">


                <label for='fecha_nac'>Fecha nacimiento (dd-mm-yyyy)</label>
                <input type='text' id='fecha_nac' name='fecha_nac' placeholder="Fecha nacimiento">

                <label for='fecha_alta'>Fecha alta (dd-mm-yyyy)</label>
                <input type='text' id='fecha_alta' name='fecha_alta' placeholder="Fecha alta">
            </div>

            <div class='derecha'>
                <label for='telefono'>Teléfono</label>
                <input type='text' id='telefono' name='telefono' placeholder="Teléfono">

                <label for='ciudad'>Ciudad</label>
                <input type='text' id='ciudad' name='ciudad' placeholder="Ciudad">

                <label for='codigo_p'>Código postal</label>
                <input type='text' id='codigo_p' name='codigo_p' placeholder="Código postal" style="width: 150px;">

                <label for='email'>Correo electrónico</label>
                <input type='text' id='email' name='email' placeholder="Correo electrónico">


                <label for='saldo'>Saldo</label>
                <input type='text' id='saldo' name='saldo' placeholder="Saldo" style="width: 150px;">

                <label for='select_tipo'>Tipo usuario</label>
                <div class="styled-select" id="select_tipo">
                    <select name="tipo" id="tipo"> 
                        <option>Administrador</option>
                        <option>Normal</option>
                    </select>
                </div>                
            </div>
            <input type="submit" name="enviar" id="enviar" value="Nuevo usuario" class="button button-blue">
        </div>
    </form>

    <div id='div_dni'>
        <span id='tit_dni'>DNI</span>
        <div class="styled-select">

            <!--onchange="alert(this.options[this.selectedIndex].text);"--> 
            
            <select size="1" name="select_dni">
                <?php
                if (isset($arrData) && !empty($arrData)) {
                    foreach ($arrData as $usuarios) {
                        ?>

                        <option value='<?php echo $usuarios[0] ?>'><?php echo $usuarios[0] ?></option>

                        <?php
                    }
                } else {
                    echo 'Data Not Found';
                }
                ?>
            </select>
        </div>
    </div>
</div>    
