<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Frameset//EN">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <style>
            <!--.aviso { color: #ff0000; font-family: arial; font-size: 10pt; fontstyle:italic }-->
        </style>
    </head>
    <body>
        <?php
        include_once('../validaors/valida.php');
     
        //recoge datos
        function recoge($var) {
            $tmp = (isset($_REQUEST[$var])) ? strip_tags(trim(htmlspecialchars($_REQUEST[$var]))) : '';
            if (get_magic_quotes_gpc())
                $tmp = stripslashes($tmp);
            return $tmp;
        }

        // asiganamos a cada campo un nombre y una variable

        if (isset($_POST['enviar'])) {
            $nombre = recoge('nombre');
            $edad = recoge('edad');
            $dni = recoge('dni');
            $email = recoge('correo');
            $nombreOk = FALSE;
            $edadOk = FALSE;
            $emailOk = FALSE;
            $dniOk = FALSE;
          
// comprobamos si nombre esta vacio o es correcto
            $dniOk = valid_dni($dni);
            if ($nombre == "")
                print "<p class=\"aviso\">No ha escrito su nombre</p>\n";
            else
                $nombreOk = TRUE;


            // comprobamos si el dni esta vacio o es correcto
            if ($dni === "")
                print "<p class=\"aviso\">No ha escrito su dni</p>\n";
            elseif (!$dniOk == TRUE)
                print "<p  class=\"aviso\">dni incorrecto</p>\n";
            else
                $dniOk = TRUE;

            //comprobamos que la edad es correcta y no este vacia
            if ($edad == "")
                print "<p class=\"aviso\">No ha escrito su edad</p>\n";
            elseif (!is_numeric($edad))
                print "<p  class=\"aviso\">No ha escrito la edad como numero</p>\n";
            elseif (!ctype_digit($edad)) //is_int()
                print "<p class=\"aviso\">No ha escrito la edad como numero entero</p>\n";
            elseif ($edad < 0 || $edad > 110)
                print "<p class=\"aviso\">LA edad no esta entre 0 y 110 anyos</p>\n";
            else
                $edadOk = TRUE;

            // comprobamos correo sea correcto
            if ($email === "")
                print "<p class=\"aviso\">No ha escrito su correo</p>\n";

            elseif (!filter_var($email, FILTER_VALIDATE_EMAIL))
                print "<p class=\"aviso\">correo  incorrecto   ej : nombe@nombre.es </p>\n";
            else
                $emailOk = TRUE;
        }else {
            $nombreOk = FALSE;
            $edadOk = FALSE;
            $dniOk = FALSE;
            $emailOk = FALSE;
        }


        if (isset($_POST['enviar']) && $nombreOk && $edadOk && $emailOk && $dniOk) {
            print "<p>Su nombre es <strong>$nombre</strong></p>\n";
            print "<p>Su edad es <strong>$edad</strong></p>\n";
            print "<p>Su dni es <strong>$dni</strong></p>\n";
            print "<p>Su correo es <strong>$email</strong></p>\n";
        } else {
            ?>
            <form action="" method="POST">
                Nombre:  <input type="text" name="nombre" value="" size="20"> <br>
                Edad: <input type="text" name="edad" value="" size="20"> <br>	
                Dni: <input type="text" name="dni" value="" size="20"> <br>	
                EMAIL: <input type="text" name="correo" value="" size="20"> <br>
                <input type="submit" name="enviar" value="enviar">
            </form>
            <?php
        }
        ?>
    </body>
</html>