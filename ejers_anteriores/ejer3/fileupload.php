   <?php
include 'upload.class.php';

function fotos($param) {
    $correcto =FALSE;
if (isset($_FILES['file'])) {
    $uploaded = new Upload($_FILES['file']);
    $types  = array('image/png');
    $ext    = array('png','jpg','jpeg','gif');
    $size   = 4;
    $uploaded->validate($size, $ext, $types);
    $name = md5(microtime());
    $estado = $uploaded->save('./imatges');
    if($estado){
       $correcto=TRUE;
       $uploaded->rename($name.png);
    } else {
       
       return $correcto;
    }
}
return $correcto;
}