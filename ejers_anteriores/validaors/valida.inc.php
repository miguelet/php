<?php

function valid_dni($dni) {
    $str = trim($dni);
    $str = str_replace("-", "", $dni);
    $str = str_ireplace(" ", "", $dni);

    if (!preg_match("/^[0-9]{7,8}[a-zA-Z]{1}$/", $dni)) {
        return FALSE;
    } else {
        $n = substr($dni, 0, -1);
        $letter = substr($dni, -1);
        $letter2 = substr("TRWAGMYFPDXBNJZSQVHLCKE", $n % 23, 1);
        if (strtolower($letter) != strtolower($letter2))
            return FALSE;
    }
    return TRUE;
}

function EsTexto($texto) {
    $reg = "^[A-Za-z]+[A-Za-z0-9]*$";
    return ereg($reg, $texto);
}

function EsNumerico($texto) {
    $reg = "^[0-9]+$";
    return ereg($reg, $texto);
}


               // print "<p class=\"aviso\">correo  incorrecto   ej : nombe@nombre.es </p>\n";
           // else
     function correo($texto) {
          if(!filter_var($texto, FILTER_VALIDATE_EMAIL)){
              return FALSE;
            
          }
         return TRUE;
         
         
     }
      
    

    


function CamposCorrectos($datos, $tipos, &$errores) {
    $correctos = true;
    for ($i = 0; $i < count($datos); ++$i) {
        switch ($tipos[$i]) {
            case 'texto': $errores[$i] = !EsTexto($datos[$i]);
                break;
            case 'entero': $errores[$i] = !EsNumerico($datos[$i]);
                break;
            case 'dni': $errores[$i] = !valid_dni($datos[$i]);
                break;
            case 'correo': $errores[$i] = !correo($datos[$i]);
                break;
        }
        if ($errores[$i] === true)
            $correctos = false;
    }
    return $correctos;
}








	function validar_Form(){
		$error='';
		$filtro = array(
			'nombre' => array(
				'filter'=>FILTER_VALIDATE_REGEXP,
				'options'=>array('regexp'=>'/^.{4,20}$/')
			),
			
			'edad' => array(
				'filter'=>FILTER_VALIDATE_INT,
				'options'=>array('regexp = "^[0-9]+$"')
			),
			
			'dni' => array(
				'filter'=>FILTER_CALLBACK,
				'options'=>'valid_dni'
			),
			
		
			'correo' => array(
				'filter'=>FILTER_CALLBACK,
				'options'=>'validatemail'
			)
		);
		
		$resultado=filter_input_array(INPUT_POST,$filtro);
		if(!$resultado['nombre']){
			$error='Usuario debe tener de 4 a 20 caracteres';
		}elseif(!$resultado['edad']){
			$error='edad comprendida de 0 a 99';
		}elseif(!$resultado['dni']){
			$error='dni ( 8 numeros y 1 letra), y tambien valido';
		}elseif(!$resultado['correo']){
			$error='El email debe contener de 5 a 50 caracteres y debe ser un email valido';
		}else{
			 return $return=array('resultado'=>true,'error'=>$error,'datos'=>$resultado);
		};
		return $return=array('resultado'=>false , 'error'=>$error,'datos'=>$resultado);
	};

	function validatemail($email){
			$email = filter_var($email, FILTER_SANITIZE_EMAIL);
			if(filter_var($email, FILTER_VALIDATE_EMAIL)){
				if(filter_var($email, FILTER_VALIDATE_REGEXP, array('options' => array('regexp'=> '/^.{5,50}$/')))){
					return $email;
				}
			}
			return false;
	}
?>
