<?php
	/* Clase encargada de gestionar las conexiones a la base de datos */
	class BD {
		private $servidor;
		private $usuario;
		private $password;
		private $base_datos;
		private $link;
		private $stmt;
		private $array;
		static $_instance;

		/* La funci�n construct es privada para evitar que el objeto pueda ser creado mediante new */
		private function __construct() {
			include ('conf.class.php');
			$this->setConexion();
			$this->conectar();
		}

		/* M�todo para establecer los par�metros de la conexi�n */
		private function setConexion() {
			$conf = conf::getInstance();
			$this->servidor = $conf->getHostDB();
			$this->base_datos = $conf->getDB();
			$this->usuario = $conf->getUserDB();
			$this->password = $conf->getPassDB();
		}

		/* Evitamos el clonaje del objeto. Patr�n Singleton */
		private function __clone() {
		}

		/* Funci�n encargada de crear, si es necesario, el objeto. Esta es la funci�n que debemos llamar desde fuera de la clase para instanciar el objeto, y as�, poder utilizar sus m�todos */
		public static function getInstance() {
			if (!(self::$_instance instanceof self))
				self::$_instance = new self();
			return self::$_instance;
		}

		/* Realiza la conexi�n a la base de datos. */
		private function conectar() {
			$this->link = new mysqli($this->servidor, $this->usuario, $this->password);
			mysqli_select_db($this->link, $this->base_datos);
		}

		public function ejecutar($sql) {
			$this->stmt = mysqli_query($this->link, $sql);
			$checkData = mysqli_num_rows($this->stmt);
			$arrPeli = array();

			if ($checkData > 0) {
				while ($row = mysqli_fetch_row($this->stmt))
					$arrPeli[] = $row;
			}
			return $arrPeli;
		}

		public function consulta($sql) {
			$this->stmt = mysqli_query($this->link, $sql);
			if (!$this->stmt) {
				echo 'MySQL Error: ' . mysqli_error();
				exit;
			}
			return $this->stmt;
		}

		/* M�todo para obtener una fila de resultados de la sentencia sql */
		public function obtener_fila($stmt, $fila) {
			if ($fila == 0) {
				$this->array = mysqli_fetch_array($stmt);
			} else {
				mysql_data_seek($stmt, $fila);
				$this->array = mysqli_fetch_array($stmt);
			}
			return $this->array;
		}

		//Devuelve el �ltimo id del insert introducido
		public function lastID() {
			return mysqli_insert_id($this->link);
		}

	}
?>