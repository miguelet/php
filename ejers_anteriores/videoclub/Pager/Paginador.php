<?php

class Paginador {

    public function __construct() {
        
    }

    //Recibe el total de resultados de algo y cuantos resultados queremos mostrar por página.
    public function configurar($totalItems, $perPage) {
        $pager_options = array(
            //'mode'       => 'Sliding',   
            'mode' => 'Jumping',
            'perPage' => $perPage, // Total rows to show per page
            'delta' => 4,
            //'nextImg'    => '<img src="next.png"  />',
            //'prevImg'    => '<img src="prev.png"  />',
            //'spacesBeforeSeparator' => 1,
            'separator' => ' - ',
            'totalItems' => $totalItems,
        );
       
        $pager = @ Pager::factory($pager_options);

        list($from, $to) = $pager->getOffsetByPageId();
     
        $from = $from - 1;
       
        $perPage = $pager_options['perPage'];
   
        $data = array('from' => $from, 'perPage' => $perPage, 'pager' => $pager);
        return $data;
    }


    public function pintar_paginador($pager) {
        
        echo '<div class="pager" id="pager_links">';
        echo $pager->links;
        echo '</div>';
        echo '<script>document.getElementById("div_peliculas").appendChild(document.getElementById("pager_links"));</script>';
    }

}

?>
