<div id="div_contacto">
    <p class="titulo_div_general">Contacto</p>

    <form action="index.php?CONTROLADOR=controlador_contacto&function=enviarEmail" method="POST" onsubmit="return validacion_con()">

        <div class='formulario_contacto'>
            <div id="div_nombre">
                <label for="nombre_con">Nombre</label>
                <input type="text" id="nombre_con" name="nombre_con" placeholder="Nombre">
                <span class="aviso_error" id="error_nombre" style="visibility: hidden">Introduce un nombre válido</span>
            </div>

            <div id="div_correo">
                <label for="correo_con">Correo electrónico</label>
                <input type="text" id="correo_con" name="correo_con" placeholder="Correo electrónico">
                <span class="aviso_error" id="error_correo" style="visibility: hidden">Introduce un correo electrónico válido</span>
            </div>

            <div id="div_mensaje">
                <label for="mensaje_con">Mensaje</label>
                <textarea rows="8" cols="40" id="mensaje_con" name="mensaje_con" placeholder="Introduce tu mensaje aquí..."></textarea>
                <span class="aviso_error" id="error_mensaje" style="visibility: hidden">Introduce un mensaje en el área de texto</span>
            </div>
            
            <input type="submit" name="enviar" id="enviar" value="Enviar" class="button button-blue">
        </div>    

    </form>

</div>