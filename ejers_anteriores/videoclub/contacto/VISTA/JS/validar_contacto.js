function validacion_con() {
    var nombre = document.getElementById("nombre_con");
    var correo = document.getElementById("correo_con");
    var mensaje = document.getElementById("mensaje_con");

    var reg_name = /^([a-z ñáéíóú]{2,60})$/i;
    var reg_correo = /^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$/;

    /* NOMBRE */
    if (nombre.value === "" || nombre.value === null || !reg_name.test(nombre.value)) {
        nombre.style.borderColor = "red";
        nombre.focus();
        document.getElementById("error_nombre").style.visibility = "visible";
        return false;
    } else {
        nombre.style.borderColor = "";
        document.getElementById("error_nombre").style.visibility = "hidden";
    }
    
    /* EMAIL */
    if (correo.value === "" || correo.value === null || !reg_correo.test(correo.value)) {
        correo.style.borderColor = "red";
        correo.focus();
        document.getElementById("error_correo").style.visibility = "visible";
        return false;
    } else {
        correo.style.borderColor = "";
        document.getElementById("error_correo").style.visibility = "hidden";
    }
    
    if(mensaje.value === "" || mensaje.value === null){
        mensaje.style.borderColor = "red";
        mensaje.focus();
        document.getElementById("error_mensaje").style.visibility = "visible";
        return false;
    }else{
        mensaje.style.borderColor = "";
        document.getElementById("error_mensaje").style.visibility = "hidden";
    }
    
    return true;
}