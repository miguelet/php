<?php
	require("rutas.php");

	session_start();

	/*** por defecto index.php carga el controlador peliculas ***/
	if (!empty($_GET['CONTROLADOR'])) {
		$CONTROLADOR = $_GET['CONTROLADOR'];
	} else {
		$CONTROLADOR = 'controlador_peliculas';
	}

	if (!empty($_GET['function'])) {
		$function = $_GET['function'];
	} else {
		$function = 'listar';
	}

	$CONTROLADOR = $CONTROLADOR;
	$modulo = explode("_", $CONTROLADOR); 
	$rutaCon = SITE_ROOT . $modulo[1] . '/CONTROLADOR/';
	$fn = $rutaCon . $CONTROLADOR . '.php';

	if (file_exists($fn)) {
		require_once($fn);
		$controladorClass = $CONTROLADOR;
		if (!method_exists($controladorClass, $function)) {
			die($function . ' funcion no encontrada');
		}

		$obj = new $controladorClass;
		$obj->$function();
	} else {
		die($CONTROLADOR . ' controlador no encontrado');
	}
?>