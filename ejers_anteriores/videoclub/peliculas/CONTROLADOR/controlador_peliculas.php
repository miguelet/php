<?php
	require("libreria/common.inc.php");
	require("Pager/Paginador.php");
	require("Pager/Pager.php");

	class controlador_peliculas {
		function listar() {
			$rutaM = $_SERVER['DOCUMENT_ROOT'] . '/videoclub/peliculas/MODELO/';
			$rutaV = $_SERVER['DOCUMENT_ROOT'] . '/videoclub/peliculas/VISTA/HTML/';
			$rutaH = $_SERVER['DOCUMENT_ROOT'] . '/videoclub/VISTA/HTML/';

			loadView($rutaH, "header.php");
			// obtener numero total de peliculas en la BD
			$arrValue = loadModel($rutaM, "modelo_peliculas", "count_peliculas");
			
			$paginador = new Paginador();
			$paginadorC = $paginador->configurar($arrValue[0][0], 8);
			$from = $paginadorC['from'];
			$perPage = $paginadorC['perPage'];
			$arrArgument = array(0 => $from, 1 => $perPage);
			
			$p = loadModel($rutaM, "modelo_peliculas", "mostrarListaPeliculas", $arrArgument);
			loadView($rutaV, "lista_peliculas.php", $p);
			$paginador->pintar_paginador($paginadorC['pager']);
			loadView($rutaH, "footer.php");
		}

		function listar_detalles() {
			$rutaM = $_SERVER['DOCUMENT_ROOT'] . '/videoclub/peliculas/MODELO/';
			$rutaV = $_SERVER['DOCUMENT_ROOT'] . '/videoclub/peliculas/VISTA/HTML/';
			$rutaH = $_SERVER['DOCUMENT_ROOT'] . '/videoclub/VISTA/HTML/';

			loadView($rutaH, "header.php");
			$id = $_GET['id'];
			$arrArgument = array(0 => $id);
			$arrValue = loadModel($rutaM, "modelo_peliculas", "peliculaDetalles", $arrArgument);
			loadView($rutaV, 'detalles_pelicula.php', $arrValue);
			loadView($rutaH, "footer.php");
		}

		function lista_ordenada() {
			$rutaM = $_SERVER['DOCUMENT_ROOT'] . '/videoclub/peliculas/MODELO/';
			$rutaV = $_SERVER['DOCUMENT_ROOT'] . '/videoclub/peliculas/VISTA/HTML/';
			$rutaH = $_SERVER['DOCUMENT_ROOT'] . '/videoclub/VISTA/HTML/';

			loadView($rutaH, "header.php");
			$genero = $_GET["genero"];
			$arrValue = loadModel($rutaM, "modelo_peliculas", "peliculas_ordenadas", $genero);
			loadView($rutaV, 'peliculas_ordenadas.php', $arrValue);
			loadView($rutaH, "footer.php");
		}
	}
?>