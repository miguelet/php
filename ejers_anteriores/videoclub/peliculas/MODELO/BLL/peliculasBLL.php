<?php
	require("peliculas/MODELO/DAO/peliculasDAO.php");
	require ("MODELO/Conexion/BD.class.php");
	
	class peliculasBLL {
		function listar_Cuadros_BLL($arrArgument) {
			$bd = BD::getInstance();
		   
			$new_list = new cuadrosDAO();
			return $new_list->listarPeliculasDAO($bd, $arrArgument);
		}

		function detallesPeliculaBLL($arrArgument) {
			$bd = BD::getInstance();

			$detalle_peli = new PeliculasDAO();
			return $detalle_peli->detallesPeliculaDAO($bd, $arrArgument);
		}

		function peliculasOrdenadasBLL($genero) {
			$bd = BD::getInstance();

			$lista_peli = new PeliculasDAO();
			return $lista_peli->peliculasOrdenadasDAO($bd, $genero);
		}

		function count_pelisBLL() {
			$bd = BD::getInstance();

			$count = new PeliculasDAO();
			return $count->count_pelisDAO($bd);
		}
	}
