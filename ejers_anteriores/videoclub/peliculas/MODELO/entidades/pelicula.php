<?php
	class pelicula {
		private $id_pelicula;
		private $titulo;
		private $director;
		private $fecha_estreno;
		private $genero;
		private $imagen_portada;
		private $duracion;
		private $actor_principal;
		private $sinopsis;
		private $idioma;
		private $url_trailer;

		public function __construct($id_pelicula, $titulo, $director, $fecha_estreno, $genero, $imagen_portada, $duracion, $actor_principal, $sinopsis, $idioma, $url_trailer) {
			$this->id_pelicula = $id_pelicula;
			$this->titulo = $titulo;
			$this->director = $director;
			$this->fecha_estreno = $fecha_estreno;
			$this->genero = $genero;
			$this->imagen_portada = $imagen_portada;
			$this->duracion = $duracion;
			$this->actor_principal = $actor_principal;
			$this->sinopsis = $sinopsis;
			$this->idioma = $idioma;
			$this->url_trailer = $url_trailer;
		}

		public function __get($property) {
			if (property_exists($this, $property)) {
				return $this->$property;
			}
		}

		public function __set($property, $value) {
			if (property_exists($this, $property)) {
				$this->$property = $value;
			}
		}

		public function __toString() {
			return  "ID: " . $this->id_pelicula . "\n" .
					"Título: " . $this->titulo . "\n" .
					"Director: " . $this->director . "\n" .
					"Fecha estreno: " . $this->fecha_estreno . "\n" .
					"Género: " . $this->genero . "\n" .
					"Imágen portada: " . $this->imagen_portada . "\n" .
					"Duración: " . $this->duracion . "\n" .
					"Actor principal: " . $this->actor_principal . "\n" .
					"Sinopsis: " . $this->sinopsis . "\n" .
					"Idioma: " . $this->idioma . "\n" .
					"URL tráiler: " . $this->url_trailer;
		}
	}
