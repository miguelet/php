<?php 
	require("peliculas/MODELO/BLL/peliculasBLL.php");

	class modelo_peliculas {
		function mostrarListaPeliculas($arrArgument){
			$list = new peliculasBLL();
			return $list->listarPeliculasBLL($arrArgument);
		}
		
		function peliculaDetalles($arrArgument){	
			$new_detalle = new peliculasBLL();
			return $new_detalle->detallesPeliculaBLL($arrArgument);
	   }
	   
	   function peliculas_ordenadas($genero){
			$peliculas_ordenadas = new peliculasBLL();
			return $peliculas_ordenadas->peliculasOrdenadasBLL($genero);
	   }
	   
	   function count_peliculas(){
			$count = new peliculasBLL();
			return $count->count_pelisBLL();
	   }
	}
