<div id="div_peliculas">
    <p class="titulo_div_general">Últimas novedades</p>
    <?php    
    if (isset($arrData) && !empty($arrData)) {
        //print_r($arrData);
        foreach ($arrData as $peliculas) {
            //echo $peliculas[1];
            ?>
            <div class="caja_peli">
                
                <div id="div_img">
                    <?php 
                    $rutaImg = $peliculas[5];
                    ?>
                    <img src=<?php echo $rutaImg ?> />
                </div>

                <div class="div_titulo">
                    <!--<a href='<?php ?> <?php ?>'><?php echo $peliculas[0] ?> - <?php /*echo $peliculas[1]*/ ?></a>-->
                    <a href='<?php echo SITE_PATH?>index.php?CONTROLADOR=controlador_peliculas&function=listar_detalles&id=<?php echo $peliculas[0];?>'><?php echo utf8_encode($peliculas[1]);?></a>
                </div>
                
                <div id="div_precio">
                    <span>Precio: <?php echo $peliculas[11] ?> € /dia</span>
                </div>
            </div>
            <?php
        }
    } else {
        echo 'Data Not Found';
    }
    ?>
</div>    
