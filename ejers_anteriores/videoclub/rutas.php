<?php
	define('DOC_ROOT', $_SERVER['DOCUMENT_ROOT']);
	define('SITE_ROOT', DOC_ROOT . '/videoclub/');
	define('SITE_PATH', 'http://' . $_SERVER['HTTP_HOST'] . '/videoclub/');
	define('IMAGE_PATH', SITE_PATH . 'VISTA/IMG/');
	define('CSS_PATH', SITE_PATH . 'VISTA/CSS/');
	define('JS_PATH', SITE_PATH . 'VISTA/JS/');
	define('LIBRARY_ROOT', SITE_ROOT . 'libreria/');
	define('VIEW_PATH', SITE_ROOT . 'VISTA/HTML/');
	define('MODEL_PATH', SITE_ROOT . 'MODELO/');
	define('CONTROLLER_PATH', SITE_ROOT . 'CONTROLADOR/');

	/*** modulo peliculas ***/
	define('PELI_IMAGE_PATH', SITE_PATH . 'peliculas/VISTA/IMG');
	define('PELI_CSS_PATH', SITE_PATH . 'peliculas/VISTA/CSS/');
	define('PELI_JS_PATH', SITE_PATH . 'peliculas/VISTA/JS/');
	define('PELI_LIBRARY_ROOT', SITE_ROOT . 'peliculas/libreria/');
	define('PELI_VIEW_PATH', SITE_ROOT . 'peliculas/VISTA/HTML/');
	define('PELI_MODEL_PATH', SITE_ROOT . 'peliculas/MODELO/');
	define('PELI_CONTROLLER_PATH', SITE_ROOT . 'peliculas/CONTROLADOR/');

	/** modulo usuarios **/
	define('USER_JS_PATH', SITE_PATH . 'usuarios/VISTA/JS/');

	/** modulo contacto **/
	define('CON_JS_PATH', SITE_PATH . 'contacto/VISTA/JS/');