<?php
	require("libreria/common.inc.php");
	require("usuarios/MODELO/BLL/usuariosBLL.php");

	class controlador_usuarios {
		function mostrar_formulario() {
			$rutaH = $_SERVER['DOCUMENT_ROOT'] . '/videoclub/VISTA/HTML/';
			$rutaM = $_SERVER['DOCUMENT_ROOT'] . '/videoclub/usuarios/MODELO/';
			$rutaV = $_SERVER['DOCUMENT_ROOT'] . '/videoclub/usuarios/VISTA/HTML/';

			loadView($rutaH, "header.php");
			$arrValue = loadModel($rutaM, "modelo_usuarios", "cargarDNIusuario");
			loadView($rutaV, 'form_usuario.php', $arrValue);
			loadView($rutaH, "footer.php");
		}

		function alta_usuario() {
			$rutaM = $_SERVER['DOCUMENT_ROOT'] . '/videoclub/usuarios/MODELO/';
			if ((isset($_REQUEST['enviar']))) {
				$dni = $_POST['dni'];
				$nick = $_POST['nick'];
				$nombre = $_POST['nombre'];
				$password = $_POST['pass'];
				$apellido = $_POST['apellido'];
				$fecha_nacimiento = $_POST['fecha_nac'];
				//edad
				$fecha_alta = $_POST['fecha_alta'];
				$telefono = $_POST['telefono'];
				$ciudad = $_POST['ciudad'];
				$codigo_p = $_POST['codigo_p'];
				$email = $_POST['email'];
				$tipo = $_POST['tipo'];
				$saldo = $_POST['saldo'];

				$arrArgument = array('dni' => $dni, 'nick' => $nick, 'nombre' => $nombre, 'password' => $password, 'apellido' => $apellido
						, 'fecha_nacimiento' => $fecha_nacimiento, 'fecha_alta' => $fecha_alta, 'telefono' => $telefono, 'ciudad' => $ciudad
						, 'codigo_p' => $codigo_p, 'email' => $email, 'tipo' => $tipo, 'saldo' => $saldo);
				$arrValue = loadModel($rutaM, "modelo_usuarios", "altaUsuario", $arrArgument);
				
				$rutaH = $_SERVER['DOCUMENT_ROOT'] . '/videoclub/VISTA/HTML/';
				$rutaM = $_SERVER['DOCUMENT_ROOT'] . '/videoclub/usuarios/MODELO/';
				$rutaV = $_SERVER['DOCUMENT_ROOT'] . '/videoclub/usuarios/VISTA/HTML/';
				
				loadView($rutaH, "header.php");
				$arrValue = loadModel($rutaM, "modelo_usuarios", "cargarDNIusuario");
				loadView($rutaV, 'form_usuario.php', $arrValue); 
				loadView($rutaH, "footer.php");
				
			} else {
				//loadView($rutaH, "header.php");
			}
		}
		
		function login_usuario(){
			if ((isset($_REQUEST['enviar']))) {
				$nick = $_POST['nick_login'];
				$password = $_POST['pass_login'];
				
				$rutaM = $_SERVER['DOCUMENT_ROOT'] . '/videoclub/usuarios/MODELO/';
				$rutaV = $_SERVER['DOCUMENT_ROOT'] . '/videoclub/usuarios/VISTA/HTML/';
				
				$arrArgument = array('nick' => $nick, 'password' => $password);
				
				// $arrValue guarda 0 o 1
				// 0 si el usuario y contraseña introducidos por el usuario no existe en la BD
				// 1 si existen esos datos en la BD
				$arrValue = loadModel($rutaM, "modelo_usuarios", "loginUsuario", $arrArgument);
				//loadView($rutaV, '', $arrValue);
				//print_r($arrValue);
				
			} else {
				//loadView($rutaH, "header.php");
			}
		}
	}
