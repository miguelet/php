<?php
	require ("MODELO/Conexion/BD.class.php");
	require("usuarios/MODELO/DAO/usuariosDAO.php");

	class usuariosBLL {
		function cargarDNIusuarioBLL() {
			$bd = BD::getInstance();
			
			$lista_dni = new usuariosDAO();
			return $lista_dni->cargarDNIusuarioDAO($bd);
		}

		function altaUsuarioBLL($arrArgument) {
			$bd = BD::getInstance();
			
			$new_user = new usuariosDAO();
			return $new_user->altaUsuarioDAO($bd, $arrArgument);
		}
		
		function loginUsuarioBLL($arrArgument){
			$bd = BD::getInstance();
			
			$new_login = new usuariosDAO();
			return $new_login->loginUsuarioDAO($bd, $arrArgument); 
		}
	}
