function validacion() {
    // Comprueba si es un DNI correcto (entre 5 y 8 letras seguidas de la letra que corresponda).
    // Acepta NIE (Extranjeros con X, Y o Z al principio)
    function isDNI(dni) {
        var numero, let, letra;
        var expresion_regular_dni = /^[XYZ]?\d{5,8}[A-Z]$/;

        dni = dni.toUpperCase();
        if (expresion_regular_dni.test(dni) === true) {
            numero = dni.substr(0, dni.length - 1);
            numero = numero.replace('X', 0);
            numero = numero.replace('Y', 1);
            numero = numero.replace('Z', 2);
            let = dni.substr(dni.length - 1, 1);
            numero = numero % 23;
            letra = 'TRWAGMYFPDXBNJZSQVHLCKET';
            letra = letra.substring(numero, numero + 1);
            if (letra != let) {
                //alert('Dni erroneo, la letra del NIF no se corresponde');
                return false;
            } else {
                //alert('Dni correcto');
                return true;
            }
        } else {
            //alert('DNI erroneo, formato no válido');
            return false;
        }
    }/* fin funcion comprobar dni correcto */

    /************ expresiones regulares para validar **************/
    var reg_pass = /^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\S+$).{8,}$/;
    /****************************************************************
     ^                # start-of-string
     (?=.*[0-9])       # a digit must occur at least once
     (?=.*[a-z])       # a lower case letter must occur at least once
     (?=.*[A-Z])       # an upper case letter must occur at least once
     (?=.*[@#$%^&+=])  # a special character must occur at least once
     (?=\S+$)          # no whitespace allowed in the entire string
     .{8,}             # anything, at least eight places though
     $                 # end-of-string 
    ****************************************************************/
    var reg_name = /^([a-z ñáéíóú]{2,60})$/i;
    var reg_apellido = /^[a-zA-z]+([ '-][a-zA-Z]+)*/;
    var reg_fecha = /^\d{2}\-\d{2}\-\d{4}$/;
    var reg_telefono = /^[0-9]{2,3}-? ?[0-9]{6,7}$/;
    var reg_codigo = /^([1-9]{2}|[0-9][1-9]|[1-9][0-9])[0-9]{3}$/;
    var reg_email = /^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$/;
    var reg_float = /^([0-9])*[.]?[0-9]*$/;
    /********** fin expresiones regulares ********/

    var dni = document.getElementById("dni");
    var nick = document.getElementById("nick");
    var pass = document.getElementById("pass");
    var nombre = document.getElementById("nombre");
    var apellido = document.getElementById("apellido");
    var fecha_nac = document.getElementById("fecha_nac");
    var fecha_alta = document.getElementById("fecha_alta");
    var telefono = document.getElementById("telefono");
    var ciudad = document.getElementById("ciudad");
    var codigo = document.getElementById("codigo_p");
    var email = document.getElementById("email");
    var saldo = document.getElementById("saldo");

    /* DNI */
    if (dni.value === "" || dni.value === null || isDNI(dni.value) !== true) {
        dni.style.borderColor = "red";
        dni.focus();
        return false;
    } else {
        dni.style.borderColor = "";
    }

    /* USUARIO */
    if (nick.value === "" || nick.value === null) {
        nick.style.borderColor = "red";
        nick.focus();
        return false;
    } else {
        nick.style.borderColor = "";
    }

    /* CONTRASEÑA */
    if (pass.value === "" || pass.value === null || pass.value.length < 8 || !reg_pass.test(pass.value)) {
        pass.style.borderColor = "red";
        pass.focus();
        alert("-Mínimo 8 carácteres\n\-Carácteres especiales\n\-Mínimo una minuscula y una mayúscula\n\-Sin espacios");
        return false;
    } else {
        pass.style.borderColor = "";
    }

    /* NOMBRE */
    if (nombre.value === "" || nombre.value === null || !reg_name.test(nombre.value)) {
        nombre.style.borderColor = "red";
        nombre.focus();
        return false;
    } else {
        nombre.style.borderColor = "";
    }

    /* APELLIDO */
    if (apellido.value === "" || apellido.value === null || !reg_apellido.test(apellido.value)) {
        apellido.style.borderColor = "red";
        apellido.focus();
        return false;
    } else {
        apellido.style.borderColor = "";
    }

    /* FECHA NACIMIENTO */
    if (fecha_nac.value === "" || fecha_nac.value === null || !reg_fecha.test(fecha_nac.value)) {
        fecha_nac.style.borderColor = "red";
        fecha_nac.focus();
        return false;
    } else {
        fecha_nac.style.borderColor = "";
    }

    /* FECHA ALTA */
    if (fecha_alta.value === "" || fecha_alta.value === null || !reg_fecha.test(fecha_alta.value)) {
        fecha_alta.style.borderColor = "red";
        fecha_alta.focus();
        return false;
    } else {
        fecha_alta.style.borderColor = "";
    }

    /* TELEFONO */
    if (telefono.value === "" || telefono.value === null || !reg_telefono.test(telefono.value)) {
        telefono.style.borderColor = "red";
        telefono.focus();
        return false;
    } else {
        telefono.style.borderColor = "";
    }

    /* CIUDAD */
    if (ciudad.value === "" || ciudad.value === null || !reg_name.test(ciudad.value)) {
        ciudad.style.borderColor = "red";
        ciudad.focus();
        return false;
    } else {
        ciudad.style.borderColor = "";
    }

    /* CODIGO POSTAL */
    if (codigo.value === "" || codigo.value === null || !reg_codigo.test(codigo.value)) {
        codigo.style.borderColor = "red";
        codigo.focus();
        return false;
    } else {
        codigo.style.borderColor = "";
    }

    /* EMAIL */
    if (email.value === "" || email.value === null || !reg_email.test(email.value)) {
        email.style.borderColor = "red";
        email.focus();
        return false;
    } else {
        email.style.borderColor = "";
    }

    /* SALDO USUARIO */
    if (saldo.value === "" || saldo.value === null || !reg_float.test(saldo.value)) {
        saldo.style.borderColor = "red";
        saldo.focus();
        return false;
    } else {
        saldo.style.borderColor = "";
    }
    return true;
}

function validar_login() {
    var nick = document.getElementById("nick_login");
    var pass = document.getElementById("pass_login");

    /* USUARIO */
    if (nick.value === "" || nick.value === null) {
        nick.style.borderColor = "red";
        nick.focus();
        return false;
    } else {
        nick.style.borderColor = "";
    }
    
    /* CONTRASEÑA */
    if (pass.value === "" || pass.value === null) {
        pass.style.borderColor = "red";
        pass.focus();
        return false;
    } else {
        pass.style.borderColor = "";
    }
    
    return true;
}