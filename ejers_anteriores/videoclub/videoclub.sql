-- phpMyAdmin SQL Dump
-- version 3.5.0
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 13-12-2013 a las 12:16:51
-- Versión del servidor: 5.1.49
-- Versión de PHP: 5.3.3

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `videoclub`
--
CREATE DATABASE `videoclub` DEFAULT CHARACTER SET utf8 COLLATE utf8_spanish_ci;
USE `videoclub`;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `peliculas`
--

CREATE TABLE IF NOT EXISTS `peliculas` (
  `id` varchar(10) COLLATE utf8_spanish_ci NOT NULL DEFAULT '',
  `titulo` varchar(100) COLLATE utf8_spanish_ci NOT NULL DEFAULT '',
  `director` varchar(100) COLLATE utf8_spanish_ci NOT NULL DEFAULT '',
  `fecha_estreno` varchar(50) COLLATE utf8_spanish_ci NOT NULL DEFAULT '00-00-0000',
  `genero` varchar(50) COLLATE utf8_spanish_ci NOT NULL DEFAULT '',
  `imagen_portada` varchar(500) COLLATE utf8_spanish_ci NOT NULL DEFAULT '',
  `duracion` varchar(10) COLLATE utf8_spanish_ci NOT NULL DEFAULT '0',
  `actor_principal` varchar(50) COLLATE utf8_spanish_ci NOT NULL DEFAULT '',
  `sinposis` varchar(20000) COLLATE utf8_spanish_ci NOT NULL DEFAULT '',
  `idioma` varchar(100) COLLATE utf8_spanish_ci NOT NULL DEFAULT '',
  `url_trailer` varchar(200) COLLATE utf8_spanish_ci NOT NULL DEFAULT '0',
  `precio` float NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `peliculas`
--

INSERT INTO `peliculas` (`id`, `titulo`, `director`, `fecha_estreno`, `genero`, `imagen_portada`, `duracion`, `actor_principal`, `sinposis`, `idioma`, `url_trailer`, `precio`) VALUES
('1', 'Guerra mundial Z', 'Marc Forster', '02-06-2013', 'Acción', 'http://localhost:8080/videoclub/peliculas/VISTA/IMG/gm.jpg', '122', 'Brad Pitt', 'Cuando el mundo comienza a ser invadido por una pandemia de muertos vivientes, el experto investigador de las Naciones Unidas Gerry Lane (Brad Pitt) intentará evitar lo que podría ser el fin de la civilización en una carrera contra el tiempo y el destino. La destrucción a la que se ve sometida la raza humana le hace recorrer el mundo entero buscando respuestas sobre cómo parar la horrible epidemia que amenaza a toda la humanidad, intentando salvar las vidas de millones de desconocidos así como la de su propia familia.', 'Español', 'http://www.youtube.com/watch?v=xPYk9E4XzXg', 2.5),
('10', 'Objetivo: La Casa Blanca', 'Antoine Fuqua', '10-05-2013', 'Thriller', 'http://localhost:8080/videoclub/peliculas/VISTA/IMG/cas.jpg', '119', 'Gerard Butler', 'Tras un accidente en el que sólo consigue salvar la vida del Presidente Asher, el agente del Servicio Secreto Mike Banning decide dejar su puesto para trabajar en el Departamento del Tesoro. Pero, cuando un comando norcoreano liderado por Kang ataca la Casa Blanca y toma como rehenes al Presidente y a su equipo, Banning se verá obligado a entrar de nuevo en acción.', 'Español', 'http://www.youtube.com/watch?v=WZioBi7Powk', 1.35),
('11', 'El Mensajero', 'Ric Roman Waugh', '22-02-2013', 'Acción', 'http://localhost:8080/videoclub/peliculas/VISTA/IMG/men.jpg', '112', 'Jon Bernthal', '"EL MENSAJERO" Dwayne "The Rock" Johnson nos muestra su lado mas humando en el papel de un padre que recibe un duro golpe cuando condenan a su hijo adolescente a una pena de treinta años de cárcel por un delito relacionado con el narcotráfico. Para conseguir que reduzcan su condena, intenta ponerse en contacto con un importante traficante.', 'Español', 'http://www.youtube.com/watch?v=arLus-AyVuM', 1.95),
('12', 'Oblivion', 'Joseph Kosinski', '10-04-2013', 'Ciencia Ficción', 'http://localhost:8080/videoclub/peliculas/VISTA/IMG/obl.jpg', '126', 'Tom Cruise', 'Tom Cruise vuelve a protagonizar una película de ciencia ficción que discurre en el año 2073. Hace más de 60 años la Tierra fue atacada; se ganó la guerra, pero la mitad del planeta quedó destruido, y todos los seres humanos fueron evacuados. Jack Harper (Tom Cruise), un antiguo marine, es uno de los últimos hombres que la habitan. Es un ingeniero de Drones que participa en una operación para extraer los recursos vitales del planeta.', 'Español', 'http://www.youtube.com/watch?v=hkfrcpsTi4Q', 2.35),
('13', 'After Earth', 'M. Night Shyamalan', '28-06-2013', 'Ciencia ficción', 'http://localhost:8080/videoclub/peliculas/VISTA/IMG/aft.jpg', '100', 'Will Smith', 'Will Smith vuelve a protagonizar una película con su hijo Jaden Smith. Tras una serie de cataclismos que forzaron a la humanidad a abandonar la Tierra, Nova Prime se convirtió en su nuevo hogar. Tras una larga misión fuera de ese planeta, el legendario general Cypher Raige regresa en compañía de su hijo Kitai. En medio de una tormenta de asteroides, la nave se avería y se estrella contra la Tierra, lugar desconocido y peligroso en el que todos los seres vivos no tienen más objetivo que eliminar a los hombres. Como Cypher ha resultado herido, Kitai debe recorrer ese mundo hostil en busca de la baliza de rescate.', 'Español', 'http://www.youtube.com/watch?v=ugecEDjhAn8', 2.1),
('14', 'Iron Man 3', 'Shane Black', '14-04-2013', 'Ciencia Ficción', 'http://localhost:8080/videoclub/peliculas/VISTA/IMG/im.jpg', '130', 'Robert Downey Jr.', 'Robert Downey Jr. vuelve a encanar al descarado y brillante empresario Tony Stark (Iron Man), que se enfrentará a un enemigo cuyo poder no conoce límites. Cuando Stark comprende que su enemigo ha destruido su universo personal, se embarca en una angustiosa búsqueda para encontrar a los responsables.', 'Español', 'http://www.youtube.com/watch?v=6Cl8PmVm3YE', 1.1),
('2', 'Fast & Furious 6', 'Justin Lin', '24-05-2013', ' Acción', 'http://localhost:8080/videoclub/peliculas/VISTA/IMG/ff.jpg', '130', 'Vin Diesel', 'Desde que Dom (Vin Diesel) y Brian (Paul Walker) destruyeron el imperio de un mafioso y se hicieron con cien millones de dólares, se encuentran en paradero desconocido; no pueden regresar a casa porque la ley los persigue. Entretanto, Hobbs (Dwayne Johnson) ha seguido la pista por una docena de países a una banda de letales conductores mercenarios, cuyo cerebro (Luke Evans) cuenta con la inestimable ayuda de la sexy Letty (Michelle Rodriguez), un viejo amor de Dom que éste daba por muerta.', 'Español', 'http://www.youtube.com/watch?v=iiY5RKePSxk', 3),
('3', 'The Host', 'Andrew Niccol', '22-03-2013 ', 'Ciencia Ficción', 'http://localhost:8080/videoclub/peliculas/VISTA/IMG/th.jpg', '125', 'Saoirse Ronan', 'La Tierra ha sido invadida por unos seres que se alojan en el cuerpo de los hombres y controlan sus mentes. Para Wanderer, la criatura que habita el cuerpo de Melanie, no es fácil acostumbrarse a soportar emociones, sentimientos y recuerdos demasiado intensos, pero la principal dificultad consiste en que Melanie lucha por conservar el control de su mente llenándola con recuerdos de Jared, el hombre que ama.', 'Español', 'http://www.youtube.com/watch?v=Gu4kGA7b4ZY', 3.5),
('5', 'El Hobbit', 'Peter Jackson', '28-11-2012 ', 'Aventura', 'http://localhost:8080/videoclub/peliculas/VISTA/IMG/hob.jpg', '169', 'Martin Freeman', 'La aventura sigue el viaje del protagonista Bilbo Bolsón, que se embarca en la recuperación del tesoro y del reino enano de Erebor arrebatados por el terrible dragón Smaug. Alcanzado de repente por el mago Gandalf el Gris, Bilbo se encuentra acompañado de trece enanos dirigidos por el legendario guerrero Thorin Escudo de roble.', 'Español', 'http://www.youtube.com/watch?v=P59fZy9Muo0', 1.2),
('6', ' Resacón en Las Vegas 3', 'Todd Phillips', ' 31-05-2013', 'Comedia', 'http://localhost:8080/videoclub/peliculas/VISTA/IMG/res.jpg', '100', 'Bradley Cooper', 'La pandilla de Bradley Cooper vuelve a protagonizar la saga de "Resacón". Tras el trauma de la muerte de su padre, Alan es llevado por sus amigos a un centro especializado, pero en el camino se topan con más de una sorpresa. Esta vez no hay boda. No hay fiesta de graduación. ¿Qué puede ir mal? Pues que cuando estos chicos salen a la carretera, la suerte está echada...', 'Español', 'http://www.youtube.com/watch?v=68_va6G3Tlk', 1.2),
('7', 'Exorcismo en Georgia', 'Tom Elkins', '14-08-2013  ', 'Terror', 'http://localhost:8080/videoclub/peliculas/VISTA/IMG/exo.jpg', '100', 'Abigail Spencer', 'Poco después de instalarse en su nueva casa de Georgia, una pareja descubre que su hija mantiene misteriosos encuentros con personas que sólo ella parece ver. La situación se complica cuando también los padres son testigos de extraños fenómenos alrededor de la casa, que sugieren un escalofriante misterio que se ha mantenido en secreto durante generaciones.', 'Español', 'http://www.youtube.com/watch?v=9T7Xafb5rhQ', 0.9),
('8', 'Saw VII', 'Kevin Geutert', '29-10-2010   ', 'Terror', 'http://localhost:8080/videoclub/peliculas/VISTA/IMG/saw.jpg', '90', 'Kevin Bell', 'Un grupo de supervivientes de los juegos de Jigsaw se reunirán para buscar el apoyo del gurú de autoayuda y superviviente Bobby Dagen, un hombre que tiene un oscuro secreto en el que quiere desatar una nueva oleada de terror', 'Español', 'http://www.youtube.com/watch?v=V1IwmygKN6c', 1.75),
('9', 'Lo imposible', 'Juan Antonio Bayona', '11-10-2012   ', 'Drama', 'http://localhost:8080/videoclub/peliculas/VISTA/IMG/imp.jpg', '113', 'Naomi Watts', 'Maria (Naomi Watts), Henry (Ewan McGregor) y sus tres hijos comienzan sus vacaciones de invierno en Tailandia. En la mañana del 26 de diciembre, la familia se relaja en la piscina después del día de Navidad cuando el mar, convertido en un enorme y violento muro de agua negra, invade el recinto del hotel. Maria solo tiene tiempo de gritar "¡Henry, los niños!" antes de ser engullida por la ola.', 'Español', 'http://www.youtube.com/watch?v=aurSS82DvCI', 2.1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE IF NOT EXISTS `usuarios` (
  `dni` varchar(10) NOT NULL DEFAULT '',
  `nick` varchar(50) NOT NULL DEFAULT '',
  `password` varchar(50) NOT NULL DEFAULT '',
  `nombre` varchar(50) NOT NULL DEFAULT '',
  `apellidos` varchar(50) NOT NULL DEFAULT '',
  `fecha_nacimiento` varchar(50) NOT NULL DEFAULT '00-00-0000',
  `edad` int(10) NOT NULL DEFAULT '0',
  `fecha_alta` varchar(50) NOT NULL DEFAULT '00-00-0000',
  `telefono` int(11) NOT NULL DEFAULT '0',
  `ciudad` varchar(50) NOT NULL DEFAULT '',
  `codigo_postal` int(5) NOT NULL DEFAULT '0',
  `email` varchar(50) NOT NULL DEFAULT '',
  `tipo` varchar(30) NOT NULL DEFAULT '',
  `saldo` float NOT NULL DEFAULT '0',
  PRIMARY KEY (`dni`,`nick`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`dni`, `nick`, `password`, `nombre`, `apellidos`, `fecha_nacimiento`, `edad`, `fecha_alta`, `telefono`, `ciudad`, `codigo_postal`, `email`, `tipo`, `saldo`) VALUES
('X7409272Y', 'ioanG19', 'ioangalan', 'Ioan', 'Galan', '19-05-1993', 20, '20-02-2013', 697884932, 'Ontinyent', 46870, 'ioandaw@gmail.com', 'Administrador', 15);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
