<?php
 
/**
 * Este proyecto esta bajo la licencia
 * Creative Commons Atribucion-NoComercial-LicenciarIgual 3.0
 * ver http://creativecommons.org/licenses/by-nc-sa/3.0/deed.es
 */
 
 
// Datos para la base de datos.
 
/**
 * El motor de la base de datos, puede ser
 * 'mysql', 'postgresql', 'orale', etc.
 */
define('BD_MOTOR', 'mysql');
 
/**
 * El host.
 */
define('BD_HOST', '127.0.0.1');
 
/**
 * Algunos host compartidos tienen un prefijo para sus bases de datos.
 */
define('BD_PREFIJO_NOM', '');
 
/**
 * El nombre de la base de datos del sistema.
 */
define('BD_PRINCIPAL_NOM', 'miguelet');
 
/**
 * El nombre del usuario.
 */
define('BD_USUARIO_NOM', 'miguelet');
 
/**
 * La clave del usuario.
 */
define('BD_USUARIO_CLAVE', 'miguelet');
 
/**
 * Texto que se agrega a los hash para aumentar su seguridad.
 * En el caso de que se transporte el sistema y se use el instalador, es
 * necesario copiar este valor, de otro modo las contraseñas no funcionarán.
 */
define('HASH_SALT', 'd+ñsdop'); // Aún no implementado.
 
?>