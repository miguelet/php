<?php
 
/**
 * Este proyecto esta bajo la licencia
 * Creative Commons Atribucion-NoComercial-LicenciarIgual 3.0
 * ver http://creativecommons.org/licenses/by-nc-sa/3.0/deed.es
 */
 
// Importar la clase BdSentencia.
require "bdsentencia.php";	

/**
 * BD es una capa de abstracción para usar bases de datos de manera más cómoda.
 * Utiliza PDO.
 *
 * @author <a mailto="rodrigo.gonzlez@gmail.com">Rodrigo González</a>
 */
class bd extends PDO {
 
    /**
     * El constructor.
     * @param <string> $nombre el nombre de la base de datos, en el caso de que
     * esté vacío toma el valor del archivo de configuración (configuracion.php)
     * correspondiente a BD_PRINCIPAL_NOM.
     */
    public function BD($nombre = BD_PRINCIPAL_NOM) {
 
        // (Data Source Name)
        $dsn = BD_MOTOR . ':host=' . BD_HOST . ';dbname=' . BD_PREFIJO_NOM . $nombre;
 
        // Instancear la conexión con la base de datos.
        parent::__construct($dsn, BD_USUARIO_NOM, BD_USUARIO_CLAVE);
 
        // Asigna la clase BdSentencia para crear los objetos que representen
        // sentencias y que heredan de PDOStatement.
        parent::setAttribute(PDO::ATTR_STATEMENT_CLASS, array('BdSentencia', array($this)));
 
        // Hace que se lancen errores, en caso de que se produzcan.
        parent::setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
 
        // Cambiar el juego de caracteres de la conexión a UTF-8, no se
        // hace a través de MYSQL_ATTR_INIT_COMMAND por un bug en PHP 3.0 y 3.1
        parent::exec('SET NAMES \'utf8\'');
 
    } 
 
    /**
     * Prepara una sentencia SQL
     * @param <string> $sql una sentencia SQL.
     */
    public function preparar($sql) {
        return parent::prepare($sql);
    }
 
    /**
     * Envía una consulta no preparada a la base de datos. Equivale a la
     * función query.
     * @param string $sql la consulta SQL.
     * @return <BdSentencia> el objeto que representa la respuesta.
     */
    public function consultar($sql) {
        return parent::query($sql);
    }
 
    /**
     * Ejecuta una sentencia SQL. No se use para SELECT, para tales casos
     * está consultar, o preparar, para preparar sentencias.
     * @param <string> $sql la sentencia SQL.
     * @return <int> el numero de filas afectadas por esta acción.
     */
    public function ejecutarRapido($sql) {
        return parent::exec($sql);
    }
 
}
 
?>