<?php
 
/**
 * Este proyecto esta bajo la licencia
 * Creative Commons Atribucion-NoComercial-LicenciarIgual 3.0
 * ver http://creativecommons.org/licenses/by-nc-sa/3.0/deed.es
 */
  
/** 
 * Representa una sentencia preparada según la clase BD.
 *
 * @author <a mailto="rodrigo.gonzlez@gmail.com">Rodrigo González</a>
 */
class bdsentencia extends PDOStatement {
 
    /**
     * Una referencia al objeto BD.
     * @var <BD> la base de datos.
     */
    private $pdo;
 
    /**
     * Constructor.
     * @param <BD> $pdo una referencia a la base de datos.
     */
    protected function BdSentencia($pdo) {
        $this->pdo = $pdo;
    }
 
    /**
     * Ejecuta una sentencia preparada, si se le pasan argumentos entonces estos
     * se reemplazan en los '?' de la sentencia preparada en el mismo orden.
     * @param <string [,string[,...]]> los argumentos a reemplazar.
     * @return <bool> true si la acción fue exitosa, false caso contrario.
     */
    public function ejecutar() {
        // Si hay argumentos entonces pasarselos como array al método nativo
        // execute.
        if (func_num_args() > 0) {
            return parent::execute(func_get_args());
        }
        return parent::execute();
    }
 
    /**
     *
     * @return <string[]> una matriz que representa la tabla consultada en la
     * base de datos.
     */
    public function getMatriz() {
        return parent::fetchAll(PDO::FETCH_ASSOC);
    }
    /**
     *
     * @return <object[]> un vector de objetos en donde cada posición es un
     * objeto que representa una fila de la tabla (producto de la consuta a la
     * base de datos), y cada atributo de los objetos son los correspondientes a
     * las columnas de dicha tabla.
     */
    
     public function recuperarclase($clase) {
       return parent::fetchAll(PDO::FETCH_INTO,"$clase");
      
    }
    
    
   public function getObjetos() {
       return parent::fetchObject();
      
    }
    //  public function getObjetos( $clase)
   // { 
   //       return parent::fetchAll(PDO::FETCH_CLASS,$clase);
        
   // }
 
    /**
     *
     * @return <int> el número de filas de la consulta.
     */
    public function contarFilas() {
        return parent::rowCount();
    }
 
}
 
?>