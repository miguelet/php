<?php

//require DAO_CUADRO_PATH . 'cuadrosDAO.php';

   class cuadros_bll {

       public function __construct() {
           
       }

       function listar_Cuadro_BLL() {
           $bd = new bd();
           $new_list = new cuadros_dao();
           return $new_list->listar_cuadro_dao($bd);
       }

       function borrar_cuadro_bll($cuadro) {
           $bd = new bd();

           $new_list = new cuadros_dao();

           $new_list->borrar_cuadro_dao($bd, $cuadro);
       }

       function modificar_Cuadro_BLL($cuadro) {
           $bd = new bd();

           $new_list = new cuadros_dao();

           $new_list->modificar_cuadro_dao($bd, $cuadro);
       }

       function anyadir_Cuadro_bll($cuadro) {
           $bd = new bd();

           $new_list = new cuadros_dao();

           $new_list->anyadir_cuadro_dao($bd, $cuadro);
       }

       function listar_cuadro_bll_modificar($cuadro) {
           $bd = new bd();

           $new_list = new cuadros_dao();

           return $new_list->listar_cuadro_dao_modificar($bd, $cuadro);
       }

   }
   