<?php

class cuadro {

    private $id_cuadro;
    private $titulo;
    private $artista;
    private $genero;
    private $imagen;
    private $alto;
    private $ancho;
    private $descripcion;
    private $precio;

    public function __construct($id_cuadro, $titulo, $artista, $genero, $imagen, $alto, $ancho, $descripcion, $precio) {
        $this->id_cuadro = $id_cuadro;
        $this->titulo = $titulo;
        $this->artista = $artista;
        $this->genero = $genero;
        $this->imagen = $imagen;
        $this->alto = $alto;
        $this->ancho = $ancho;
        $this->descripcion = $descripcion;
        $this->precio = $precio;
    }

    public function __get($property) {
        if (property_exists($this, $property)) {
            return $this->$property;
        }
    }

    public function __set($property, $value) {
        if (property_exists($this, $property)) {
            $this->$property = $value;
        }
    }

    public function __toString() {
        return " ID : " . $this->id_cuadro . "\n";
        " titulo : " . $this->titulo . "\n";
        " artista : " . $this->artista . "\n";
     
        " imagen : " . $this->imagen . "\n";
        " alto : " . $this->alto . "\n";
        " ancho : " . $this->ancho . "\n";
        " descripcion : " . $this->descripcion . "\n";
        " precio : " . $this->precio . "\n";
    }

}
