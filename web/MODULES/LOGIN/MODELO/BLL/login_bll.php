<?php

  // require DAO_LOGIN_PATH . 'loginDAO.php';
//require_once ENTIDADES_LOGIN_PATH . 'login.php';
   class login_bll {

       function buscar_nick_BLL($nick) {
             $bd = new bd();

           $new_list = new login_dao();

           return $new_list->buscar_nick_DAO($bd, $nick);
       }
       
       
       
        function buscar_correo_BLL($correo) {
             $bd = new bd();

           $new_list = new login_dao();

           return $new_list->buscar_correo_DAO($bd, $correo);
       }

       function alta_prov_BLL($arr_value) {
           $bd = new bd();

           $new_list = new login_dao();

           return $new_list->alta_prov_DAO($bd, $arr_value);
       }

       function alta_usuario_BLL($arrArgument) {
           $bd = new bd();
           $new_user = new login_dao();
           return $new_user->alta_usuario_DAO($bd, $arrArgument);
       }

   

       function recuperar_BLL($arr_value) {

           $bd = new bd();
           $new_list = new login_dao();
           return $new_list->recuperar_DAO($bd, $arr_value);
       }

       function validar_token_BLL($token) {

           $bd = new bd();
           $new_list = new login_dao();
           return $new_list->validar_token_DAO($bd, $token);
       }
       
       
        function get_pass_BLL($correo) {

           $bd = new bd();
           $new_list = new login_dao();
           return $new_list->get_pass_DAO($bd, $correo);
       }
       
            function validar_pass_BLL($nick) {

           $bd = new bd();
           $new_list = new login_dao();
           return $new_list->validar_pass_DAO($bd, $nick);
       }
       
       
       
       function cambiar_contrasenia_BLL($login){
           
           $bd = new bd();
           $new_list = new login_dao();
           return $new_list->cambiar_contrasenia_DAO($bd, $login);
           
       }
       
       
       
         function cambiar_tipo_BLL($login){
           
           $bd = new bd();
           $new_list = new login_dao();
           return $new_list->cambiar_tipo_DAO($bd, $login);
           
       }
       
       
   
       function guardar_token_BLL($aleatorio){
           
           $bd = new bd();
           $new_list = new login_dao();
           return $new_list->guardar_token_DAO($bd, $aleatorio);
       }
         function recuperar_email_BLL($token){
             
               $bd = new bd();
           $new_list = new login_dao();
           return $new_list->recuperar_email_DAO($bd, $token);
             
         }
          function validar_token_BLL_pass($token) {

           $bd = new bd();
           $new_list = new login_dao();
           return $new_list->validar_token_DAO_PASS($bd, $token);
       }
      }
   