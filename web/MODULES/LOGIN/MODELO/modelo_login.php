<?php

   // require_once BLL_LOGIN_PATH . 'loginBLL.php';
   //  require_once BD_CONEXION1 . 'BD.php';
   // require_once CONFIG_PATH . 'BD/configuracion.php';
   // require_once ENTIDADES_LOGIN_PATH . 'login.php';
   //require BLL_LOGIN_PATH . 'loginBLL_JSON.php';
   //require BLL_LOGIN_PATH . 'loginBLL_JSON_get.php';

   class modelo_login {

       function buscar_nick($nick) {
           $new_list = new login_bll();
           return $new_list->buscar_nick_BLL($nick);
       }

       function buscar_correo($correo) {
           $new_list = new login_bll();
           return $new_list->buscar_correo_BLL($correo);
       }

       function alta_Usuario($arrArgument) {
           $new_user = new login_bll();
           return $new_user->alta_usuario_BLL($arrArgument);
       }

       function validar_token($token) {
           $new_list = new login_bll();
           return $new_list->validar_token_BLL($token);
       }

       function recupera_alta($arrArgument) {
           $get_user = new login_bll();

           return $get_user->recuperar_BLL($arrArgument);
       }

       function altaUsuario_prov($arrArgument) {
           $new_user = new login_bll();
           return $new_user->alta_prov_BLL($arrArgument);
       }

       function get_pass($nick) {
           $new_list = new login_bll();
           return $new_list->get_pass_BLL($nick);
       }

       function validar_pass($nick) {
           $new_list = new login_bll();
           return $new_list->validar_pass_BLL($nick);
       }

       function cambiar_contrasenia($login) {
           $new_list = new login_bll();
           $new_list->cambiar_contrasenia_BLL($login);
       }

       function cambiar_tipo($login) {
           $new_list = new login_bll();
           $new_list->cambiar_tipo_BLL($login);
       }

       function guardar_token($aleatorio) {
           $new_list = new login_bll();
           $new_list->guardar_token_BLL($aleatorio);
       }

       function recuperar_email($token) {

           $new_list = new login_bll();
           return $new_list->recuperar_email_BLL($token);
       }

       function validar_token_pass($token) {
           $new_list = new login_bll();
           return $new_list->validar_token_BLL_pass($token);
       }

       function pilla_dades_json() {

           $add = new login_bll_json_get();
           return $add->recuperar_json_bll();
       }

       function listar_login_JSON() {

           $add = new login_bll_json();
           $add->listar_login_BLL_JSON();
       }

       function cambia_dades_json() {

           $add = new login_bll_json_get();
           return $add->cambia_dades_json_bll();
       }

       function alta_json() {

           $add = new login_bll_json_get();
           return $add->alta_json_bll();
       }

       function correo_JSON() {

           $add = new login_bll_json_get();
           return $add->correo_JSON_BLL();
       }

       function pass_JSON() {

           $add = new login_bll_json_get();
           return $add->pass_JSON_BLL();
       }

   }
   