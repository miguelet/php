<?php

   /*    * * nullify any existing autoloads ** */
   spl_autoload_register(null, false);
   spl_autoload_extensions('.php,.class.php.inc.php,.conf.php');
   spl_autoload_register('loadClasses');

   function loadClasses($className) {
       /*        * ***autoload _ LOGIN**** */
       if (file_exists('MODULES/LOGIN/MODELO/BLL/' . $className . '.php')) { //require("peliculas/MODELO/BLL/peliculasBLL.php");
           set_include_path('MODULES/LOGIN/MODELO/BLL/');
           spl_autoload($className);
       } elseif (file_exists('MODULES/LOGIN/MODELO/DAO/' . $className . '.php')) { //require("peliculas/MODELO/BLL/peliculasBLL.php");
           set_include_path('MODULES/LOGIN/MODELO/DAO/');
           spl_autoload($className);
       } elseif (file_exists('MODULES/LOGIN/CONTROLADOR/' . $className . '.php')) { //require("peliculas/MODELO/BLL/peliculasBLL.php");
           set_include_path('MODULES/LOGIN/CONTROLADOR/');
           spl_autoload($className);
       } elseif (file_exists('MODULES/LOGIN/MODELO//ENTIDADES/' . $className . '.php')) { //require("peliculas/MODELO/entidades/peliculas.php");
           set_include_path('MODULES/LOGIN/MODELO/ENTIDADES/');
           spl_autoload($className);
       } elseif (file_exists('MODULES/LOGIN/MODELO/' . $className . '.php')) { //require("peliculas/MODELO/peliculas_modelo.php");
           set_include_path('MODULES/LOGIN/MODELO/');
           spl_autoload($className);



         /*
          * fins asi el login
          * 
          */
       } elseif (file_exists('MODELO/CONEXION/' . $className . '.php')) { //require ("MODELO/Conexion/BD.class.php");
           set_include_path('MODELO/CONEXION/');
           spl_autoload($className);
       }
       //config BD i altres

       
        
       /*        * ***autoload _ CUADRO**** */
       else if (file_exists('MODULES/CUADRO/MODELO/BLL/' . $className . '.php')) { //require("peliculas/MODELO/BLL/peliculasBLL.php");
           set_include_path('MODULES/CUADRO/MODELO/BLL/');
           spl_autoload($className);
       } elseif (file_exists('MODULES/CUADRO/MODELO/DAO/' . $className . '.php')) { //require("peliculas/MODELO/BLL/peliculasBLL.php");
           set_include_path('MODULES/CUADRO/MODELO/DAO/');
           spl_autoload($className);
       } elseif (file_exists('MODULES/CUADRO/MODELO//ENTIDADES/' . $className . '.php')) { //require("peliculas/MODELO/entidades/peliculas.php");
           set_include_path('MODULES/CUADRO/MODELO/ENTIDADES/');
           spl_autoload($className);
       } elseif (file_exists('MODULES/CUADRO/MODELO/' . $className . '.php')) { //require("LOGIN/MODELO/peliculas_modelo.php");
           set_include_path('MODULES/CUADRO/MODELO/');
           spl_autoload($className);
       } elseif (file_exists('MODULES/CUADRO/CONTROLADOR/' . $className . '.php')) { //require("LOGIN/MODELO/peliculas_modelo.php");
           set_include_path('MODULES/CUADRO/CONTROLADOR');
           spl_autoload($className);
       }
       /*
        * fins asi el cuadro!!
        */
   }
	
	
