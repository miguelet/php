<?php

require("./rutes.php");
require ("./autoload.php");
require  LIBRARY_ROOT . 'common.inc.php';
require_once LIBRARY_ROOT . 'valida.inc.php';
require_once LIBRARY_ROOT . 'email.php';
require_once LIBRARY_ROOT . 'pilla_dades.inc.php';
require_once CONFIG_PATH . 'BD/configuracion.php';

session_start();
date_default_timezone_set('Europe/Madrid');
//	session_start();
/* * * por defecto index.php carga el controlador cuadros ** */
if (!empty($_GET['CONTROLADOR'])) {
    $CONTROLADOR = $_GET['CONTROLADOR'];
} else {
    $CONTROLADOR = 'CONTROLADOR_CUADRO';
}

if (!empty($_GET['function'])) {
    $function = $_GET['function'];
} else {
    $function = 'listar';
}

$modulo = explode("_", $CONTROLADOR);
$rutaCon = SITE_ROOT . 'MODULES/' . $modulo[1] . '/CONTROLADOR/';
$fn = $rutaCon . $CONTROLADOR . '.php';

if (file_exists($fn)) {
    require_once($fn);
    $controladorClass = $CONTROLADOR;
    if (!method_exists($controladorClass, $function)) {
        die($function . ' funcion no encontrada');
    }
    $obj = new $controladorClass;
   call_user_func(array($obj,$function)); //////////////////// ROUTING/PROXY ////////////////////
} else {
    die($CONTROLADOR . ' controlador no encontrado');
}
        
 