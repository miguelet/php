<?php

/* * * Rutes generals ** */
define('DOC_ROOT', $_SERVER['DOCUMENT_ROOT']);
define('SITE_ROOT', DOC_ROOT . '/yolanda/web/');
define('SITE_PATH', 'http://' . $_SERVER['HTTP_HOST'] . '/yolanda/web/');
define('IMAGE_PATH', SITE_PATH . 'VISTA/IMG/');
define('CSS_PATH', SITE_PATH . 'VISTA/CSS/');
define('LIBRARY_ROOT', SITE_ROOT . 'LIBRERIAS/');
define('LIBRARY_ROOT_EXTERNAS', SITE_ROOT . 'librerias_externas/encriptar/');
define('VIEW_PATH', SITE_ROOT . 'VISTA/');
define('UPLOAD_PATH', SITE_ROOT . 'LIBRERIAS/UPLOAD/');
define('BD_CONEXION', SITE_ROOT . 'MODELO/CONEXION/');
define('MEDIA_PATH_IMG', SITE_PATH . 'MEDIA/');
define('CONFIG_PATH', SITE_ROOT . 'CONFIG/');
//define('MODEL_PATH', SITE_ROOT . 'MODELO/');
//define('CONTROLLER_PATH', SITE_ROOT . 'CONTROLADOR/');
//define('JS_PATH', SITE_PATH . 'VISTA/JS/');
define('BD_CONEXION1', SITE_ROOT . 'MODELO/CONEXION/proves/');
//include_once 'alguna_ruta/BD.php';
//include_once 'alguna_ruta/configuracion.php';

/* * * Rutes a modules ** */

define('CUADRO_PATH_PATH', SITE_PATH . 'MODULES/CUADRO/');
define('CUADRO_PATH_ROOT', SITE_ROOT . 'MODULES/CUADRO/');

define('LOGIN_PATH_ROOT', SITE_ROOT . 'MODULES/LOGIN/');
define('LOGIN_SITE_PATH', SITE_PATH . 'MODULES/LOGIN/');

/* * * modulo login ** */
define('LOGIN_VISTA_PATH', LOGIN_PATH_ROOT . 'LOGIN/VISTA/');
define('BLL_LOGIN_PATH', LOGIN_PATH_ROOT . 'MODELO/BLL/');
define('DAO_LOGIN_PATH', LOGIN_PATH_ROOT . 'MODELO/DAO/');
define('ENTIDADES_LOGIN_PATH', LOGIN_PATH_ROOT . 'MODELO/entidades/');
define('LOGIN_JS_PATH', LOGIN_SITE_PATH . 'VISTA/JS/');
define('HTML_LOGIN_PATH', LOGIN_PATH_ROOT . 'VISTA/HTML/');
define('VISTA_LOGIN_PATH', LOGIN_PATH_ROOT . 'VISTA/');
define('JS_LOGIN_PATH', LOGIN_PATH_ROOT . 'VISTA/JS/');
define('MODELO_LOGIN_PATH', LOGIN_PATH_ROOT . 'MODELO/');

/** modulo usuarios * */
//	define('USER_JS_PATH', SITE_PATH . 'usuarios/VISTA/JS/');

/** modulo contacto * */
//	define('CON_JS_PATH', SITE_PATH . 'contacto/VISTA/JS/');

/* * * modulo cuadros ** */
define('BLL_CUADRO_PATH', CUADRO_PATH_ROOT . 'MODELO/BLL/');
define('DAO_CUADRO_PATH', CUADRO_PATH_ROOT . 'MODELO/DAO/');
define('HTML_CUADRO_PATH', CUADRO_PATH_ROOT . 'VISTA/HTML/');
define('JS_CUADRO_PATH', CUADRO_PATH_PATH . 'VISTA/JS/');
define('CSS_CUADRO_PATH', CUADRO_PATH_PATH . 'VISTA/CSS/');
define('MODELO_CUADRO_PATH', CUADRO_PATH_ROOT . 'MODELO/');
define('ENTIDADES_CUADRO_PATH', CUADRO_PATH_ROOT . 'MODELO/entidades/');
// rutes DAO JSON


